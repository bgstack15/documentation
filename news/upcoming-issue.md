# Devuan News Issue $ROMAN

__Volume 03, Week $WEEK, Devuan Week $DWN__

Released $DATE

https://git.devuan.org/devuan-editors/devuan-news/wikis/past-issues/volume-03/issue-$DN

## Editorial

(Enter the editorial here and sign it. Currently the prerogative of {put
your name here})


## Comings and Goings

(OPTIONAL: any life events that may affect the project, e.g., an
upcoming get-together, vacations of a developer, expected travels and
delays...)


## Lately in Devuan

(Each thread)

### Group: devuan-packages

All packages in Devuan maintains its repository in this Gitlab group.  The group's issues is the Debian equivalent for bugs.debian.org.  Its milestones are Devuan release milestones.

All Devuan maintainers should check in and verify their Git remotes.  If your source package repository moved there, you were notified by email with instructions.  If your package was not moved, please move it there (or ask someone on #devuan-www).

This package-specific group aims at facilitating automation (continuous integration, generation of images), and to provide a one-stop shop for all Devuan packaging activity.
Additionally it cleans the namespace for future derived distributions to settle in.

### New Package: simple-netaid

Edward Bartolo (@edbarx) created a simple connection manager that aims at "portability, minimal dependencies, KISS" and "uses the services of `ifup`, `ifdown`, `iwlist`, `ifconfig`, [and] `ip` to connect to [the WiFi interface] using a backend cli program running as root."

### [Devuan on Raspberry Pi][3]

Warning Pi users: if you're trying to run Devuan on an older Pi, know that Devuan Pi image is compiled for ARMHF, aka. Pi2, not Pi0.  You can find a list of ARM-device-specific images that run ARMv7 processors and are compatible with Devuan.  Steve Litt recommends against using the Banana Pi, as it runs a System-on-Chip (SoC) by AllWinner, the infamous GPL violator. 

[3]: some-url "Migration to devuan Jessie failed on rpi-zero"

### [A front-end to pmount][4]

Inspired by Steve Litt's amounter, fsmithred has created a front-end to pmount called usbwait. The frontend is written in shell scripts based on reused code from refracta2usb with inotifywait added on. The program opens up a graphical window when a usb drive is plugged in to get your choice of drive to mount and then opens it with the default file manager. Installation is done from a tarball for now, with the author planning to produce a debian package for the scripts later on. 

[4]: https://lists.dyne.org/lurker/message/20160209.140152.42a224ec.en.html "A front-end to pmount"


## Devuan Discourse

https://talk.devuan.org/ hosts a Discourse forum instance.  You can login there with your git.devuan.org account ("sign in with gitlab").

We're looking for forum-oriented contributors to help us set the tone and populate topics.  Interested volunteers are welcome to drop by the "[Early Birds][5]" topic in Meta.

As this point, participating in Devuan Discourse only by email is not possible: you still need to use the Web interface to adjust your settings and subscribe to topics.  But [soon][6] email participation will be transparent, like using a mailing list.

[5]: https://talk.devuan.org/t/early-birds/218 "Early Birds topic on Devuan Discourse"
[6]: 

## Devuan's Not Gnome

DNG is the discussion list of the Devuan Project.

- [Subscribe to the list][subscribe]
- [Read online archives][archives]

-------------------
Read you soon!

Devuan News is made by your peers: you're [welcome to contribute][wiki]!

(move section links here when done)

[subscribe]: https://mailinglists.dyne.org/cgi-bin/mailman/listinfo/dng "Subscribe to DNG"
[archives]: https://lists.dyne.org/lurker/list/dng.en.html "Read DNG List Archive"
[upcoming]: https://git.devuan.org/devuan-editors/devuan-news/wikis/upcoming-issue "Upcoming Issue"
[wiki]: https://git.devuan.org/devuan-editors/devuan-news/wikis/home "Devuan
News"
