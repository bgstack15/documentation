We at Devuan News [DN](home) use [Holocene
Era](http://en.wikipedia.org/wiki/Holocene_calendar), also called
Human Era, as a means to indicate that we fully support the idea of
Devuan being universal, for people of all countries and religions.

Holocene Era more or less counts the years since the start of
Holocene, the current geological era, and also counts, more or less,
the years since the Neolithic Revolution and the start of
agriculture. Thus, Holocene Era gives a surprisingly accurate count of
the years of Human (Pre)History.

And it is easy to calculate: just add 10000 years to the commonly used
western Gregorian Calendar. (__Sorry people from the future, from the
past, at present we're all from the future.__)
