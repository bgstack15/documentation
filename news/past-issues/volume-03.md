# [Devuan News](..)

[Archives](home) for __volume 03__ (12016 [HE](../why-he "Holocene Era"))

This year we decided to drop the weekly bit, and release "when it's ready."
Still, we're keeping the Devuan week count, noted AD (After Devuan ;).

## January

- XX [Issue LVII](volume-03/issue-057) (unannounced)
- 31 [Issue LX](volume-03/issue-060) __α4__

[previous Volume](volume-02)
