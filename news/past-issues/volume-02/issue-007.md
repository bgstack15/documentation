# [Devuan Weekly News][1] Issue VII

__Volume 02, Week 2, Devuan Week 7__ https://git.devuan.org/Envite/devuan-weekly-news/wikis/past-issues/volume-02/issue-007

## Editorial

Welcome to Devuan Weekly News VII. Do not try to find issues V and VI,
as I was on holidays and setting up a collaborative system for
upcoming issues.

On this fifth issue, corresponding to 7th week of our project (and
thus named Issue VII), I want to give a warm welcome the first
collaborator of DWN: hellekin.

This new format is thanks to him (in fact, this whole issue is his
work). Please comment.

Noel

er Envite

DWN coordinator

## Last Week in Devuan

### [Package Submission][2]

Diffing vs. Cloning upstream packages.  Franco Lanza proposes to
maintain lightweight repositories in order to integrate changes more
easily against original packages.

Enrico Weigelt argues to mirror upstream packages as downstream
branches (not forks) which he says is what the usual tarball+patches
scheme is doing anyway.

### [Release Roadmap][3]

Release Roadmap highlights: Devuan "alpha" Jessie will not include
loginkit nor eudev by default, as they're not ready yet. Systemd and
libsystemd0 dependencies will be absent as well, and xfce4 will be the
default desktop. GNOME3 will be installable but dependent on
systemd-shim.

The repository will be populated by January 23, and the first ISO
installer is scheduled for January 31.

Devuan package maintainer candidates should send an email to Franco
Lanza <nextime@devuan.org> with Subject: "devuan package maintainer"
and specify which package they want to maintain, a little description
of their skills, and their account name on git.devuan.org.

### [vdev update and design document][4]

(missing comment)

### [Devuan Security Wishlist][5]

Godefridus Daalmans wants Devuan to add default support and
documentation for Secure Attention Key (SAK). Ctrl-Alt-SysRq-K can be
used to emulate SAK, killing all processes on the current virtual
console (including X and svgalib programs).

### [Wheezy][6]

t.j.duchene warns that Debian Wheezy is now installing systemd-logind0
as Debian has been backporting packages from Jessie. He denounced QA
pitfalls and the primacy of packager convenience over user
interest. He wonders whether the introduction of dependencies on
systemd in Wheezy is a matter of easing upgrade to Jessie, or
negligence on the part of package maintainers who see systemd as a
solution to patching existing code.

Hendrik Boom develops the thread into the genealogy of systemd in from
Squeeze on. The introduction route of systemd seems to come from
backports, dbus, and X11. Oliver Schwank got Debian packagers to
explain their logic in introducing dependencies on libsystemd0:
https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=773924
("systemd-free" means "not using systemd as init", nop code is ok)

Aldemir Akpinar jokingly suggests to rename systemd meta package in
Devuan to systemd-innocence.

### [Jessie without systemd][7]

The TRIOS team introduces their semi-rolling Debian Jessie based OS
that aims to work without Systemd.  It still uses udev at this point.
It looks very similar to Devuan, using OpenRC as the default init
system and XFCE4 as the desktop.  It targets ordinary users.

Martinx - ジェームズ introduces them to eudev so that they can remove
the last systemd-udev dependency on the systemd food chain.

## Devuan Weekly News

The weekly news dispatch is not disappearing.  We're trying to make it
a team's work, so if you're interested, please drop by \#devuan-news
and brainstorm along.

-------------------
Read you next week!

Devuan Weekly News is made by your peers: you're [welcome to contribute][wiki]!

[1]: https://git.devuan.org/Envite/devuan-weekly-news/wikis/current-issue "Current Issue"
[2]: https://lists.dyne.org/lurker/message/20150104.054540.0d4cebe5.en.html "Package Submission"
[3]: https://lists.dyne.org/lurker/message/20150104.060312.cd41e777.en.html "Release Roadmap"
[4]: https://lists.dyne.org/lurker/message/20150104.141649.71081c77.en.html "vdev Update and Design Document"
[5]: https://lists.dyne.org/lurker/message/20150105.101649.0f5ed327.en.html "Security Wishlist"
[6]: https://lists.dyne.org/lurker/message/20150105.185839.e3e1e9ec.en.html "Wheezy"
[7]: https://lists.dyne.org/lurker/message/20150108.222844.f83bac6d.en.html "Jessie Without Systemd"
[wiki]: https://git.devuan.org/Envite/devuan-weekly-news/wikis/home "Devuan Weekly News"
