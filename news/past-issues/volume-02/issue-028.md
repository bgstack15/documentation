# Devuan News Upcoming Issue XXVIII

__Volume 02, Week 23, Devuan Week 28__

Released Tuesday, 12015/06/09 [HE](why-he)

https://git.devuan.org/devuan-editors/devuan-news/wikis/past-issues/volume-02/issue-028

__This issue was not released__

## Editorial

A big thank you goes to the volunteers working on the Devuan News. *You* make this possible.

@etech3

## Lately in Devuan

### [vdev status update][3]

Update and links

Jude Nelson posted his latest update for vdev

### [New systemd release notes regarding udev][4]


New systemd release notes regarding udev.

James Powell writes that it appears udev is getting split up between several projects within systemd and GNOME. It seems gudev is being handed off in the future to GNOME and parts of udev are being pushed deeper into systemd.


### [section 5][5]

[[How to remove systemd from a Debian jessie/sid installation]]

http://without-systemd.org/wiki/index.php/How_to_remove_systemd_from_a_Debian_jessie/sid_installation


### [section 6][6]


### [section 7][7]

### [section 8][8]

+ devuan-reference
+ devuan-doc

## Devuan's Not Gnome

DNG is the discussion list of the Devuan Project.

- [Subscribe to the list][subscribe]
- [Subscribe to the feed][atom-feed]
- [Read online archives][archives]

-------------------
Read you soon!

Devuan News is made by your peers: you're [welcome to contribute][wiki]!

+ Created by Noel "@Envite" Torres
+ @hellekin (editor at large)
+ @golinux (word wrangler)
+ @lightbringer (AKA. MinceR, sentence fixaupper)
+ @DocScrutinizer05 (proofreader)
+ @etech3 (markdown master in training MMIT)

--- Links to follow ---


[3]: https://lists.dyne.org/lurker/message/20150514.032442.4939d09c.en.html "vdev status update"

[4]: https://lists.dyne.org/lurker/message/20150602.012004.3a65c787.en.html "New systemd release notes regarding udev"

[5]:

[6]:

[7]:

[8]:

[pre-alpha]: http://mirror.debianfork.org/devuan-jessie-i386-xfce-prealpha-valentine.iso

[vagrant]: https://lists.dyne.org/lurker/message/20150307.011135.a710525c.en.html

[subscribe]: https://mailinglists.dyne.org/cgi-bin/mailman/listinfo/dng "Subscribe to DNG mailing list"

[atom-feed]: http://lists.devuan.org/dwn/atom.xml "Subscribe to ATOM feed"

[archives]: https://lists.dyne.org/lurker/list/dng.en.html "Read DNG List Archive"

[Financial report]: https://lists.dyne.org/lurker/message/20150503.224708.236843dd.en.html "Financial report, 1st trimester 2015"

[upcoming]: https://git.devuan.org/devuan-editors/devuan-news/wikis/upcoming-issue "Upcoming Issue"

[wiki]: https://git.devuan.org/devuan-editors/devuan-news/wikis/home "Devuan News"

[why-he]: https://git.devuan.org/devuan-editors/devuan-news/wikis/why-he
