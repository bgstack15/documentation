# [Devuan News](home) Edition Guidelines

DN issues are released once a week, usually on Tuesday morning UTC,
to the [DNG mailing
list](https://mailinglists.dyne.org/cgi-bin/mailman/listinfo/dng).

It is written in
[MarkDown](http://daringfireball.net/projects/markdown/syntax), a wiki
syntax that reads well in plain text format.

This document describes the process of making a new issue, and
provides tips to ease consistent publication and team work.

## Working Flow

1.  During the week, until Monday, collaborate in the [upcoming issue](upcoming-issue) as you wish.
1.  On Monday the DN Team will "freeze" the upcoming issue, by launching a git branch named "polishing".
1.  During Monday, the DN Team will work on the polishing process, discussing openly on IRC but not commiting to this wiki, by finish editing the upcoming issue.
1.  At the end of the work, the DN Editor will
   1.  Write the Editorial Comment
   1.  Rename __upcoming-issue.md__ to a numbered name
   1.  Copy __issue-template.md__ to __upcoming-issue.md__
   1.  Fix Archive pages and links to "current issue"
   1.  git-merge "polishing" into "master"
   1.  Send the new issue of DN to the mailing list
1.  Rinse, and repeat.

The rolling files allows to keep the links intact in [home](home), as
redirections do not exist (yet) in Gollum.  Otherwise it would be
safer and simpler to directly use the archive URL (permalink).

## Formatting

When using Markdown, the editors need to keep in mind that the source
file will be sent to the mailing list as plain text.  Some email
clients may interpret Markdown, but it will generally read as text.

### Links

Therefore, links should use the _reference syntax_ so that they don't
impede reading the newsletter.  Instead of brackets-parenthesis to
mark the anchor and the hypertext reference respectively, we use
brackets-brackets to mark the anchor and a reference id. The reference
id is then appended at the end of the document, followed by a colon,
the target URL, and a quoted title matching the section where the link
was made.

For example:

<pre><code>
  This is \[not useful in\](https://bad.example.com/you-do-not/use/that/form) a sentence.

  But you can [use this form][2] and then append the corresponding reference:

\[2\]: https://good.example.org/this-is-right "How to link properly"
</code></pre>

Limit the links to the minimum, unless the thread is wild and you want
to skip some of it. Normally, you'd link to the first post in this
thread for this week.

### Voice

Use the active voice. Don't try to repeat the whole conversation. Try
instead to capture the main point that will let people use the link to
learn more. Copy-paste the name of the poster from the _From:_ header,
and complete in the present tense:

<pre>
  Jane Doe suggests using metapackages. John Doe points to a similar
  discussion in another list.
</pre>

### Editorial

On Monday, the DN Editor will review everything and complete the newsletter 
with an editorial.  It is the exclusive role of the coordinator to write 
this text.  We may do it collaboratively, but for now just don't bother.

## Contributing

There are three ways to participate in the next issue of DWN.

### DNG mailing list

Simply by sending email to the mailing list, you will probably appear
in the DN. Of course, constructive and informative contributions will
be given more attention.

### IRC #devuan-news

If you'd like to collaborate more regularly you're welcome to join the
\#devuan-news IRC channel of the Freenode network.

### Giving a Hand

The DN is created and maintained as a [Git
repository](https://git.devuan.org/devuan-editors/devuan-news).

You can send patches, translations, etc.  Any help is welcome.

## Releasing

On Monday, the team gathers on IRC to review the issue and make last
minute changes.  When the __upcoming-issue.md__ is ready, it is sent
to the mailing list as plain text.

The copy of what was sent to the mailing list also needs to be known
as "the current issue", and also in the past issues.  Read on to get
acquainted to the archival process.

## Archiving

Upon releasing a new issue, the __current-issue.md__ contains the
previous issue.  The same file should also have an exact copy in
__past-issues/volume-VV/issue-II.md__.  If that is not the case,
create that copy now.

Next, the file __upcoming-issue.md__ contains the content sent to the
mailing list. Now it can be moved to both __current-issue.md__ and
__past-issues/volume-VV/issue-II.md__, with VV corresponding to the
year (02 for 12015), and II corresponding to the number of weeks since
the first DN issue (07 for the first released issue of 12015).

E.g., Issue VII is at
__[past-issues/volume-02/issue-07.md](past-issues/volume-02/issue-07.md)__.
