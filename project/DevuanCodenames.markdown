# Devuan Codenames

Like debian, Devuan uses codenames to identify different releases other than numeric versioning, and like debian devuan have a release cycle involving different suites.

The 3 main suites in devuan are, exactly like in debian, stable, testing and unstable. Experimental is the 4th standard suite, used only to test new packages or packages that aren't ready (yet) to be pushed as unstable.

Any new package will go, eventually after being in experimental, in unstable. If 10 days without major issues are passed after inclusion of the package in unstable, it will be moved to testing.

Stable follow a release cycle "when it's ready", like how it happen in debian.

Also, like in debian, stable will be an alias of the actual codename of the stable release, and so will be testing for the "next stable" release. Unstable will have a fixed codename aliasing, like sid is for debian.

At the time i'm writing ( Feb. 2015 ), stable for devuan is Jessie, Ascii is the codename for the testing release.

Unstable is aliased both with "sid" codename for debian compatibility, but in Devuan the equivalent codename is "ceres". Ceres was choosen cause it's the name of the first minor planet ever discovered.

With the exception of Devuan 1.0, named "jessie" for debian compatibility, we will follow as naming scheme for releases the names of minor planets from http://www.minorplanetcenter.net/iau/lists/MPNames.html, using the first char of the name in alphabetical order.

For testing Ascii was choosen, as it has meaning in the IT world and it's a minor planet name too.
When Jessie will come the old-stable and Ascii will be the stable release, a new name starting with B will be choosen from the minorplane names list.

Ascii is also a tribute to jaromil, author of hasciicam (https://jaromil.dyne.org/journal/hasciicam.html) and one of the attributed authors of the well know bash fork bomb :(){ :|: & };: as a form of art, thanks him for the hard work he is doing in and for Devuan. Thanks Danis!