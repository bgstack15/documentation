# Testing: Ascii

## The Planet

[(3568) ASCII](http://ssd.jpl.nasa.gov/sbdb.cgi?sstr=3568) was discovered on October 17, 1936 by Marguerite Laugier at the Nice Observatory.

[releases](../releases) - previous: [jessie](jessie) - this release: [ascii](ascii) - unstable: [ceres](ceres)

