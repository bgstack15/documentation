# Devuan Codenames

Like debian, __devuan__ uses codenames to identify different releases other than numeric versioning, and like debian, __devuan__ has a release cycle involving different suites.

The 3 main suites in __devuan__ are, exactly like in debian, `stable`, `testing` and `unstable`. The fourth standard suite, `experimental`, is reserved for testing new packages, or packages that aren't yet ready for `unstable`.

Any new package will, maybe after a period in `experimental`, go to `unstable`.  After 10 days have passed without major issues following the introduction of the package in `unstable`, it will be moved to `testing`.

Like in debian, __devuan__ `stable` follows a "when it's ready" release cycle.

Following the debian tradition of aliasing codenames to release names, __devuan__ will alias `stable` and `testing` to their respective release names. The `unstable` codename will also have a fixed alias:

| Codename        | Release Name   | debian release | Notes                             |
|-----------------|----------------|----------------|-----------------------------------|
| `stable`        | [jessie][0]    | jessie         | Matching release                  |
| `testing`       | [ascii][1]     | (jessie+1)     | Independent release               |
| `unstable`      | [ceres][2]     | sid            | Fixed release name                |
| `experimental`  | n/a            | n/a            | No release, so no alias           |

[0]: releases/jessie "Devuan Jessie Release Information"
[1]: releases/ascii  "Devuan Ascii Release Information"
[2]: releases/ceres  "Devuan Ceres Release Information"

At the time of this writing ( Feb. 2015 ), `stable` for __devuan__ is __Jessie__, and `testing` release name is __Ascii__.

For compatibility with debian, __devuan__ `unstable` codename is aliased to `sid` as well as its release name [ceres](releases/ceres).  Ceres was the first 
minor planet ever discovered, and announces the pattern chosen for __devuan__ release names: after __devuan "jessie" 1.0__, named after debian's upcoming release 8.0 for compatibility, each __devuan__ release will be named after a [minor planet](http://www.minorplanetcenter.net/iau/lists/MPNames.html), using the first letter of the name in alphabetical order.  For `testing` __Ascii__ was chosen, as it has meaning in the IT world and it's a minor planet name as well.

After __devuan__ `jessie` becomes `old-stable`, and `ascii` becomes the `stable` release, a new names starting with `B` will be chosen from the [Minor Planet Center Names List](http://www.minorplanetcenter.net/iau/lists/MPNames.html).
