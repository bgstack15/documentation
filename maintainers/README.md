# Contributing to Devuan

There is no formal procedure to become a Devuan contributor. A good starting
place is to join the conversations on the #devuan-dev channel at
irc.freenode.net and subscribe to the [Devuan Developers
list](https://mailinglists.dyne.org/cgi-bin/mailman/listinfo/devuan-dev). The
community will be interested to hear your ideas, experience and the ways in
which you wish to contribute. There is also a weekly meeting (currently Thursday
at 2030UTC) which is open to everybody.

# Becoming a Package Maintainer

Sources for current (and historical) packages are kept in the
[devuan-packages](https://git.devuan.org/devuan-packages) group of our [git
repository](https://git.devuan.org). If you are looking to adopt a package,
those that are maintained by
[devuan-dev@lists.dyne.org](https://bugs.devuan.org/cgi/pkgreport.cgi?maint=devuan-dev%40lists.dyne.org)
generally have no specific maintainer and could be taken over.

Prospective Package Maintainers will also find the following useful:

 * [Devuan Packaging Guide](./PackagingGuide.md)
 * [Jenkins Autobuild Infrastructure](./JenkinsAutobuild.md)
 
