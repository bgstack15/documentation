# Jenkins Autobuild infrastructure in Devuan

Autobuild in Devuan works as easy as to open an issue.

There is a little python bot, [devuan-releasebot](https://git.devuan.org/devuan-infrastructure/devuan-releasebot), that every 5 mins in a cron list all issues assigned to him and then, parsing labels and checking permissions, start a jenkins job to build the package and to push it in the repository.

## Requisites to start a build

Before to start a build with the autobuilder, you need to be sure to match some pre-requisites:

 * having a buildable package source in devuan gitlab, with at least a branch names suites/<something> where <something> is the name of the suite to build (at the moment i write this doc, jessie, ascii, beowulf, unstable and experimental are accepted suites name).
 * having at least master or owner access to the repository on gitlab for the package to be built
 * the package should be already configured in jenkins (ask the release team for that)
 * having labels for the suites name in the gitlab project issues
 * add the "releasebot" member to the project or group, "guest" access is enough, so you can assign an issue to him.

## How to start a build

If you have all requisites to start a build, you can start it as easy as to open an issue with title "build" on the project you want to build, assign the issue to "releasebot" gitlab member, and then add labels with the name of the suite you want to build to.

At the first run of the releasebot (every 5 minutes) it will comment the issue with infos regarding if the build is queued or if there is any error in the build request, and then it will close the issue.

You can then look at [jenkins](https://jenkins.devuan.dev) (no login needed) to see if your package is being built and pushed to the repository without errors.

Jenkins also sends notifications to the #devuan-ci channel on irc.freenode.net.

## which packages are already configured in jenkins?

You can see a list of packages already configured in [jenkins source jobs](https://jenkins.devuan.dev/view/Only_sources/)

Enjoy!
