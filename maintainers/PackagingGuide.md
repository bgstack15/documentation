# Packaging Guide for Devuan

## Introduction

This file documents Devuan's packaging infrastructure, guidelines and typical
workflow.

Devuan is a derivative of Debian and much of Debian's documentation also
provides useful and relevant resources. In particular, the following are good
starting points:-

- https://wiki.debian.org/Packaging
- https://www.debian.org/doc/manuals/maint-guide/
- https://wiki.debian.org/PackagingWithGit

Devuan's source code is maintained in [git](https://git.devuan.org), builds are
performed using [jenkins](https://jenkins.devuan.dev), package repositories are
maintained with [dak](https://git.devuan.org/devuan-infrastructure/dak) and
those repositories are merged with Debian's to produce the distribution by
[amprolla](https://git.devuan.org/devuan-infrastructure/amprolla3).

The majority of Devuan's packages are used directly from Debian with no
recompilation. Only packages that require modification are forked and rebuilt.

Packaging help can be found by asking on the #devuan-dev channel on
irc.freenode.net.

## Infrastructure

Devuan's source code is managed in a git repository (GitLab as of 2020, but with
planned migration to gitea). The server is accessed at https://git.devuan.org.

Users can create their own accounts to work on packages and create merge
requests. Official Devuan packages reside in the [devuan-packages
group](https://git.devuan.org/devuan-packages) which provides the source for
Jenkins when builds are triggered.

If a user has sufficient permissions, builds can be triggered by opening a new
issue against the relevant devuan-packages/${SOURCE_PACKAGE} with the title
'build' and assigning it to releasebot. Further details can be found in
[JenkinsAutobuild](./JenkinsAutobuild.md).

## Package Categories

There are four categories of packages in Devuan:-

 1. Devuan only, not present in Debian.
 2. Forked from Debian and modified.
 3. Present in Debian but Devuan is the packaging upstream (currently only
    elogind and eudev).
 4. Used directly from Debian unchanged.

Packages in the last category will usually not be present in Devuan's git or
jenkins. However, packages can change category over time and so remnants of
previous incarnations may remain.

## Git

1. Clone a local copy of the relevant repository from
   [git.devuan.org/devuan-packages](https://git.devuan.org/devuan-packages].
2. Add Debian's SCM as another remote. The URL can be found through [Debian
   Package Tracker](https://tracker.debian.org). This is optional, but makes
   merging much easier and is encouraged.
3. Create a new branch from your chosen starting point. This is usually the
   version in Debian unstable. rmadison(1) from the devscripts package is a
   convenient way of determining current Debian versions.

	Usually packages are initially built for unstable or experimental so the
	branch should be named suites/unstable or suites/experimental accordingly.
	Sometimes packages are built to update suites that have already been
	released. In that case the branch will be suites/${CODENAME} where CODENAME
	is the release suite (jessie, ascii, beowulf, chimaera, daedelus...). The
	release codenames may optionally have -proposed appended.

	Once the branch is created and checked-out, add ```debian-branch =
	suites/unstable``` (or whatever codename is appropriate) to the file
	debian/gbp.conf. Create the file if necessary.


## Build Environment

The jenkins build pipeline uses git-buildpackage, pbuilder and cowbuilder. This
ensures more flexible and reproducible builds in a cleaner environment than
other build approaches. To avoid unexpected build failures in jenkins, you
should replicate this approach when testing. This approach also means that you
can build for suites, architectures and configurations that are incompatible
with your host sytem without conflicts.

To setup a local build environment

1. Install the required packages.

	```apt install git-buildpackage pbuilder cowbuilder```

	Some packages may require other debhelper modules to be installed (e.g.
    dh-golang). If these are required builds will fail and the build log will
    explicitly detail what is required.

2. Create a cowbuilder chroot.

	There are many ways to do this. One of the simplest is to run

	```gbp buildpackage --git-pbuilder --git-dist=ceres create --mirror http://pkgmaster.devuan.org/merged ```

	from within the git repository.

## Packaging Conventions

### debian/control

- Update Maintainers and Uploaders appropriately.
- Ensure that the Vcs-* fields contain the official Devuan URL
  (https://git.devuan.org/devuan-packages/${SOURCE_PACKAGE}).
- Add ```Origin: Devuan``` to the source stanza.
- Remove any ```Built-Using:``` field.

	As the Devuan archive (managed by DAK) is only partial, Built-Using
    dependencies are not necessarily satisfied.

### debian/gbp.conf

This file is useful for setting defaults for git-buildpackage. See gbp.conf(5).

Besides ```debian-branch``` already mentioned above, particularly useful settings include:-

- upstream-tag=upstream/v%(version)s
- debian-tag = devuan/%(version)s
- pristine-tar=False

Note that Devuan avoids the use of pristine-tar wherever possible as it fills
the repository with binary blobs. If Debian's package repository has a
pristine-tar branch you will need to prevent git-buildpackage from using it.

## Packaging Workflow

1. Review bug reports at https://bugs.devuan.org and also any issues that might
   have been opened at https://git.devuan.org/devuan-packages/${SOURCE_PACKAGE}
   and plan to address/incorporate fixes as required.

   For any bugreports that you close, add (Closes: #<bugnumber>) at the end of
   the debian/changelog line. This will automatically close the bug when the
   package is built and enters the archive.

2. Make the changes required to the source code.

	When doing this, remember that for non-native packages any changes outside
    the debian/ directory are required to be implemented as quilt patches in
    debian/patches/. This restriction does not apply to native packages.

3. Update debian/changelog.

	The general convention for package numbering is to use the Debian version
    with +devuanN appended (where N is an incremental integer).

4. Local build.

	```gbp buildpackage --git-pbuilder --git-dist=ceres```

5. Lintian

	Install devuan-lintian-profile.

	```lintian *.changes```

6. Local install.

7. Repeat from 2 until you are finished.

8. Tag the release.

	If you are working on a new package use the format
    devuan/${VERSION}. However, if there is already a convention in use that
    particular package you might prefer to continue with the existing one.

9. Publish.

	Push your branch and the tag to https://git.devuan.org. If you are working on a
    non-native package you should also push the upstream tag.

10. Merge.

	Create a merge request to https://git.devuan.org/devuan-packages or contact
    a developer with access to review and merge your changes.

11. Build for release.

	Details of triggering the Jenkins Build Pipeline can be found in
    [JenkinsAutobuild](./JenkinsAutobuild.md).

## Tips

### Updating Build Chroots

Cowbuilder chroots will need updating periodically, more often for the testing
and unstable distributions where packages change frequently. The indication that
this is required is usually build failures with 404 errors for build
dependencies, for example:

```
E: Failed to fetch http://mirrors.dotsrc.org/debian/pool/main/d/doxygen/doxygen_1.8.16-2_amd64.deb:
404  Not Found [IP: 130.225.254.116 80]
```

There are many ways of updating the chroots. The easiest is by running

```gbp buildpackage --git-pbuilder --git-dist=<distribution> update```

from the root of the git repository.

You may also find a pbuilder hook useful.

1. Configure a hook dir in ~/.pbuilderrc. For example:

	```HOOKDIR=$HOME/.pbuilder/hooks```

2. Add a executable script in that directory to update the chroot package lists at the
   beginning of the build:

   The file should be ~/.pbuilder/hooks/D05apt-update

   containing

```
#!/bin/sh
apt update
```

For further information see pbuilder(8).
