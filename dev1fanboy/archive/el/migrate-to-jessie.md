# Μετανάστευση σε Devuan Jessie
Αυτό το κείμενο περιγράφει πως να μεταναστεύσετε στη Devuan Jessie από
τη Debian.

## Εκτέλεση της μετανάστευσης
Χρειαζόμαστε να επεξεργαστούμε το κατάλογο των καθρεφτών, έτσι ώστε να
θέσουμε το αποθετήριο της Devuan ως πηγή των πακέτων.

`root@debian:~# editor /etc/apt/sources.list`

Αλλάξτε τους καθρέφτες σας σε αυτούς της Devuan, απενεργοποιώντας
(σχολιάστε τους) τους προηγούμενους της Debian.

~~~
deb http://pkgmaster.devuan.org/merged jessie main
deb http://pkgmaster.devuan.org/merged jessie-updates main
deb http://pkgmaster.devuan.org/merged jessie-security main
deb http://pkgmaster.devuan.org/merged jessie-backports main
~~~

Προτού μπορέσουμε να παραλάβουμε πακέτα από το αποθετήριο της Devuan,
πρέπει να ενημερώσουμε τα αρχεία ευρετηρίου πακέτων.


`root@debian:~# apt-get update`

Απαιτείται το κλειδί της Devuan για τη πιστοποίηση της αυθεντικότητας
των πακέτων.

`root@debian:~# apt-get install devuan-keyring --allow-unauthenticated`

Τώρα που το κλειδί της Devuan έχει εγκατασταθεί θα πρέπει να
ανανεώσετε ξανά τα ευρετήρια, έτσι ώστε τα πακέτα να πιστοποιούνται
για την αυθεντικότητά τους από δω και στο εξής.

`root@debian:~# apt-get update`

Μπορούμε τώρα να μεταναστεύσουμε στη Devuan. Εάν ερωτηθείτε, επιλέξτε
το slim ως προκαθορισμένο διαχειριστή οθόνης.

`root@debian:~# apt-get dist-upgrade`

Για την αφαίρεση του systemd ως pid1, απαιτείται μία και μόνη
επανεκκίνηση. 

`root@devuan:~# reboot`

## Εργασίες μετά τη μετανάστευση
Εάν χρησιμοποιούσατε το GNOME κάτω από Debian πριν, προτείνω την
αλλαγή του διαχειριστή συνεδρίας σε startxfce4.

`root@devuan:~# update-alternatives --config x-session-manager`

Τα συστατικά του systemd θα πρέπει τώρα να αφαιρεθούν από το σύστημα.

`root@devuan:~# apt-get purge systemd systemd-shim`

Εάν δε χρησιμοποιείται το D-Bus ενδέχεται να μπορείτε να αφαιρέσετε το
libsystemd0.

`root@devuan:~# apt-get purge libsystemd0`

Καθαρίστε τυχόν ορφανά πακέτα που απομένουν από τη προηγούμενη
εγκατάσταση της Debian.

`root@devuan:~# apt-get autoremove --purge`

Αυτή είναι η κατάλληλη στιγμή να καθαρίσετε τα παλιά αρχεία πακέτων
απομεινάρια του συστήματος Debian.

`root@devuan:~# apt-get autoclean`

---

<sub>**Αυτό το έργο διατίθεται υπό την άδεια χρήσης Creative Commons Αναφορά Δημιουργού - Παρόμοια Διανομή 4.0 Διεθνές [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.el)]. Όλα τα εμπορικά σήματα αποτελούν ιδιοκτησία των αντίστοιχων ιδιοκτητών τους. Αυτό το έργο παρέχεται "ΩΣ ΕΧΕΙ" και δεν προσφέρει ΚΑΜΙΑ απολύτως εγγύηση.**</sub>
