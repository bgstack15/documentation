# Eliminando recomendados
Este documento describe cómo configurar APT para tratar a los paquetes recomendados como opcionales y eliminarlos de
forma retroactiva. Esto permite deshacerse de aquellos paquetes opcionales que no son deseados o usados en la mayoría
de los casos. Gracias a [TheFlash] por señalar esta interesante característica de APT.

## Configuración de APT
El primer paso consiste en configurar APT para tratar a los paquetes recomendados como no importantes.

`root@devuan:~# editor /etc/apt/apt.conf.d/01lean`

Configurar APT para que los recomendados sean tratados como opcionales.

~~~
APT::Install-Recommends "0";
APT::AutoRemove::RecommendsImportant "false";
~~~

## Protección de paquetes importantes
Mientras que la mayoría de los paquetes recomendados no son importantes por naturaleza, hay algunos paquetes que se
deben proteger para que no sean eliminados ya que son necesarios.

Por seguridad y para que funcionen correctamente los navegadores y otras aplicaciones, se debe asegurar que los
certificados SSL de las autoridades certificantes de confianza siempre estén disponibles. Para ello se debe instalar
el paquete ca-certificates. Sólo evitar esto si se sabe exactamente qué se está haciendo.

`root@devuan:~# apt-get install ca-certificates`

El paquete que provee los certificados SSL es ahora marcado como instalado de manera manual, en lugar de una
dependencia o recomendado. Si el siguiente paso muestra otros paquetes que se deseen mantener instalados en el sistema,
hacer lo mismo que en el paso anterior para cada uno de ellos, antes de confirmar la eliminación de paquetes.

## Eliminar paquetes no deseados
Ahora es posible eliminar de manera retroactiva todos los paquetes que se han vuelto huérfanos por los cambios en la
configuración.

`root@devuan:~# apt-get autoremove --purge`

Ahora que se han eliminado, no es necesario mantenerlos archivados en la caché.

`root@devuan:~# apt-get autoclean`

---

<sub>Traducido por Emiliano Marini.</sub>

<sub>**Este trabajo es liberado bajo la licencia Atribución-CompartirIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)]. Todas las marcas registradas son propiedad de sus respectivos dueños. Este trabajo se provee "COMO TAL" y NO posee garantía en absoluto.**</sub>

