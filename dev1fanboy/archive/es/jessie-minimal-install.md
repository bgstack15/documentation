# Instalación mínima de Devuan Jessie
Este documento describe cómo realizar una instalación mínima de Devuan Jessie desde un dispositivo de arranque tal como un CD/DVD-ROM o un dispositivo USB.

## Prerrequisitos
Este método de instalación apunta a usuarios avanzados que desean obtener más (menos de hecho, pero ¡menos es más!) de su sistema Devuan. Por lo tanto se debe estar familiarizado con el método de instalación experto y otras tareas de administración generales, al igual que estar habituado al uso de la consola.

## Arquitecturas soportadas
* amd64
* i386

## Introducción
Se utilizará el instalador como medio para configurar el sistema de forma simple, al mismo tiempo en que se evita la instalación de la selección de paquetes por defecto. Para ello se instalarán sólo los paquetes esenciales permitidos por apt, y luego se hará un chroot a la instalación. Mediante el instalador se configurará lo básico, que será copiado a nuestra instalación al final del proceso.

## Pasos iniciales
Una vez iniciado el sistema desde el medio de arranque, comenzar la instalación utilizando el modo experto.

Seguir todos los pasos de la instalación hasta que el disco haya sido particionado y formateado, luego dejar de utilizar el instalador ya que terminaremos el resto de forma manual hasta finalizar.

Completaremos la primera etapa de la instalación con debootstrap y luego chroot al entorno de instalación para continuar desde allí. Para quienes no estén familiarizados con debootstrap, es la herramienta responsable de la instalación de los sistemas basados en Debian.

## Ejecutar debootstrap sobre el directorio de instalación
Cambiar a una terminal presionando Alt + F2 y luego enter para activar la consola.

El instalador ya ha configurado y montado las particiones, así que el disco está listo para la instalación. Confirmar ésto listando el directorio /target.

`~ # ls /target`

Antes de continuar asegurarse de tener las últimas claves gpg necesarias para debootstrap.

`~ # gpg --recv-keys 95861109`

Ahora ejecutaremos debootstrap sobre /target para completar la primera etapa de la instalación. Es importante incluir la opción --variant=minbase para obtener el conjunto mínimo de paquetes. En este punto incluiremos el editor nano para hacer el trabajo más fácil, el cual se podrá sustituir por cualquier otro de nuestra preferencia.

`~ # debootstrap --variant=minbase --include=nano jessie /target http://pkgmaster.devuan.org/merged`

## Chroot hacia el directorio /target
Para continuar la instalación es necesario hacer un 'chroot' sobre el entorno de instalación a fin de poder configurar los paquetes.

Primero es necesario hacer que /proc, /dev y /sys estén disponibles desde el entorno chroot.

`~ # mount -t proc proc /target/proc`

`~ # mount -o bind /dev /target/dev`

`~ # mount -o bind /sys /target/sys`

Ahora sí hacer el chroot hacia el entorno de instalación.

`~ # chroot /target /bin/bash`

## Eliminar paquetes no deseados
En este momento es posible lograr un sistema más mínimo eliminando aquellos paquetes que no son necesarios pero que no pueden ser eliminados durante el proceso de debootstrap.

Dependiendo de la configuración utilizada, puede no ser necesario preocuparse por la internacionalización con lo cual puede eliminarse.

`root@devuan:/# dpkg --purge debconf-i18n`

Como sólo se necesita un paquete gcc-base, es posible eliminar gcc-4.8-base en favor de gcc-4.9-base.

`root@devuan:/# dpkg --purge gcc-4.8-base`

## Configurar APT para excluir los paquetes recomendados
Los paquetes recomendados ocupan mucho espacio en el sistema en pos de agregar características que son raramente necesarias. Otra ventaja de la instalación a través de debootstrap es que es posible asegurarse de que se evite la instalación de paquetes recomendados desde un comienzo. Cabe destacar que los navegadores (y wget) requieren del paquete ca-certificates para verificar las conexiones SSL. Por ende luego se deberá instalar este paquete. ¡No olvidar este paso!

Utilizar un editor para realizar los cambios necesarios.

`root@devuan:/# sensible-editor /etc/apt/apt.conf.d/01lean`

Agregar la siguientes líneas.

~~~
APT::Install-Recommends "0";
APT::AutoRemove::RecommendsImportant "false";
~~~

## Actualizar el sistema
Antes de continuar se requiere actualizar el sistema con los últimos parches de seguridad.

Primero agregar los repositorios.

`root@devuan:/# sensible-editor /etc/apt/sources.list`

Asegurarse de que las siguientes líneas estén presentes en el archivo sources.list.

~~~
deb http://pkgmaster.devuan.org/merged jessie main
deb http://pkgmaster.devuan.org/merged jessie-updates main
deb http://pkgmaster.devuan.org/merged jessie-security main
deb http://pkgmaster.devuan.org/merged jessie-backports main
~~~

Actualizar desde los repositorios para utilizarlos.

`root@devuan:/# apt-get update`

Ahora es posible actualizar los paquetes base.

`root@devuan:/# apt-get dist-upgrade`

## Seleccionar una interfaz para debconf
Hay más de una interfaz disponible para debconf y se debe elegir una ya que aún no hay ninguna instalada. Esta es una buena oportunidad para personalizar el sistema.

### Whiptail
Whiptail es una interfaz fácil de utilizar similar a dialog, y es la interfaz a debconf por defecto para Devuan.

### Dialog
Dialog es similar a whiptail y resultará muy familiar a aquellos usuarios habituados a hacer instalaciones en modo texto.

### Readline
La interfaz readline es la más básica de todas ya que permite seleccionar opciones solo a través de respuestas numéricas. Esta interfaz utiliza un módulo de Perl, con lo cual puede ser una buena elección si el sistema tiene otros usos para Perl.

### Editor
Este método de configuración de paquetes no es una interfaz en sí misma, sino que debconf abre un editor en la primera línea relevante del archivo de configuración para el paquete instalado. Esta puede ser una buena herramienta para aprender más sobre el sistema, o tener un mayor control sobre el mismo en caso de ser necesario.

## Instalar y configurar la interfaz debconf
Una buena elección para el propósito de lograr un sistema mínimo es utilizar whiptail.

`root@devuan:/# apt-get install whiptail`

La interfaz readline es una buena alternativa para los fanáticos de la consola.

`root@devuan:/# apt-get install libterm-readline-perl-perl`

También es posible utilizar dialog si se prefiere en lugar de whiptail.

`root@devuan:/# apt-get install dialog`

A continuación, reconfigurar debconf para indicarle qué interfaz se desea utilizar a partir de ahora.

`root@devuan:/# dpkg-reconfigure debconf`

## Agregar los componentes de red
Instalar los paquetes mínimos para hacer funcionar la red.

`root@devuan:/# apt-get install netbase net-tools ifupdown`

Algunas herramientas de red opcionales pueden ser necesarias, especialmente un cliente DHCP si se necesita configurar la red automáticamente.

`root@devuan:/# apt-get install isc-dhcp-client inetutils-ping`

## Instalar el kernel Linux
A fin de poder iniciar nuestro nuevo sistema GNU/Linux será necesario instalar el kernel Linux.

```root@devuan:/# apt-get install linux-image-`dpkg --print-architecture` ```

## Instalar el gestor de inicio
Dado que tiene un tamaño de instalación menor y más soporte que LILO, utilizaremos GRUB2 como gestor de incio (*boot loader*).

Si se utilizan volúmenes lógicos será necesario instalar antes el paquete lvm2.

`root@devuan:/# apt-get install lvm2`

Si hay otro sistema operativo instalado y se desea poder iniciar desde el mismo, instalar el paquete os-prober.

`root@devuan:/# apt-get install os-prober`

Ahora instalaremos el gestor de inicio.

`root@devuan:/# apt-get install grub2`

Generalmente se desea instalar el gestor de inicio en el sector MBR del primer disco el cual, en la mayoría de los casos, se corresponde con el dispositivo /dev/sda.

## Extras opcionales
Algunos paquetes opcionales para la instalación base.

`root@devuan:/# apt-get install psmisc pciutils rsyslog less`

Esta es una sugerencia para paquetes que podrán asistir en la post-instalación si es necesario.

`root@devuan:/# apt-get install man manpages lynx irssi`

Si se necesita acceder al sistema de manera remota no olvidar instalar OpenSSH en este momento.

`root@devuan:/# apt-get install openssh-server openssh-client openssh-blacklist`

## Salir del entorno chroot (de manera limpia)
Primero salir del entorno chroot.

`root@devuan:/# exit`

Desmontar /proc, /dev y /sys.

`~ # umount /target/proc`

`~ # umount /target/dev`

`~ # umount /target/sys`

## Finalizar la instalación
Es momento de indicarle al instalador que finalice la instalación. Todos los restantes archivos de configuración serán creados en el sistema destino, con lo cual este paso no puede ser omitido.

Volver al instalador presionando Alt + F1 y saltar hasta el último paso que finaliza la instalación.

El instalador solicitará instalar el sistema base, pero se debe declinar seleccionando la opción *go back* (volver atrás). Esto también debe hacerse para el paso de instalación del gestor de inicio.

---

<sub>Traducido por Emiliano Marini.</sub>

<sub>**Este trabajo es liberado bajo la licencia Atribución-CompartirIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)]. Todas las marcas registradas son propiedad de sus respectivos dueños. Este trabajo se provee "COMO TAL" y NO posee garantía en absoluto.**</sub>

