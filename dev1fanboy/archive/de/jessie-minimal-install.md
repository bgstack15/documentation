# Devuan Jessie Minimalinstallation
Dieses Dokument beschreibt, wie Sie eine Minimalinstallation von Devuan Jessie von einem bootfähigen Laufwerk wie einem CD/DVD-ROM-Laufwerk oder USB-Stick durchführen können.

## Voraussetzungen
Diese Installationsmethode ist für fortgeschrittene Benutzer gedacht, die mehr (weniger, aber weniger ist mehr!) aus ihrem Devuan-System herausholen wollen. Sie sollten daher bereits mit der Installation von Devuan aus dem Installationsprogramm für Experten und anderen allgemeinen Verwaltungsaufgaben vertraut sein und sich mit der Konsole vertraut machen.

## Unterstützte Architekturen
* amd64
* i386

## Einführung
Wir werden den Installer dazu verwenden, das System einfach zu konfigurieren, während wir andererseits die vorkonfigurierte Standardpaketauswahl meiden. Um dies zu tun, werden wir nur die essentiellen Pakete installieren, die von apt erlaubt sind, und dann die Installation chroot. Wir werden das Installationsprogramm verwenden, um die Grundlagen zu erstellen, die am Ende dieses Prozesses in unsere Installation kopiert werden.

## Anfangsschritte
Sobald Sie von den Installationsmedien gebootet haben, starten Sie das Installationsprogramm mit der Experteninstallationsoption.

Gehen Sie alle Schritte des Installationsprogramms durch, bis der Datenträger partitioniert und formatiert wurde, und verwenden Sie das Installationsprogramm nicht weiter, bis wir die Installation abgeschlossen haben, da wir ab hier alles selber machen.

Wir werden die erste Stufe der Installation mit debootstrap abschließen, dann die Installationsumgebung chroot und von dort fortfahren. Für diejenigen, die mit debootstrap nicht vertraut sind, es ist das Tool, das für die Installation von Debian-basierten Systemenen verantwortlich ist.

## Debootstrap das Ziel
Zuerst auf ein Terminal umschalten. Drücken Sie nun Alt + F2, um dies zu tun, und drücken Sie ENTER, um die Konsole zu aktivieren.

Das Installationsprogramm hat uns bereits dabei geholfen, die Partitionen so einzurichten und zu mounten, dass die Festplatte zur Installation bereit ist. Bestätigen Sie dies, indem Sie sich die Verzeichnisstruktur von /target ansehen.

`~ # ls /target`

Bevor Sie fortfahren, vergewissern Sie sich, dass Sie über die neuesten gpg-Schlüssel verfügen, die von debootstrap benötigt werden.

`~ # gpg --recv-keys 95861109`

Jetzt werden wir den Bootvorgang auf /target abschließen, um die erste Phase der Installation abzuschließen. Es ist wichtig, die Option --variant=minbase einzuschließen, da uns dies das Minimum an Paketen bietet. Wir werden an dieser Stelle den Nano-Editor einbauen, um Ihnen das Leben zu erleichtern, den Sie nach Belieben durch Ihren Lieblings-Editor ersetzen können.

`~ # debootstrap --variant=minbase --include=nano jessie /target http://pkgmaster.devuan.org/merged`

## Chroot zum target
Um mit dem Installationsprozess fortzufahren, müssen wir in die Installationsumgebung 'chroot' gehen, damit wir weiterhin Pakete installieren und konfigurieren können.

Wir müssen zuerst proc, dev und sys für die Chroot-Umgebung verfügbar machen.

`~ # mount -t proc proc /target/proc`

`~ # mount -o bind /dev /target/dev`

`~ # mount -o bind /sys /target/sys`

Jetzt chroot in die Installationsumgebung.

`~ # chroot /target /bin/bash`

## Entfernen unerwünschter Pakete
Sie können nun ein minimaleres System erhalten, indem Sie Pakete bereinigen, die nicht benötigt werden, aber während des Bootstrap-Prozesses nicht entfernt werden können.

Abhängig von Ihrer Konfiguration ist die Internationalisierung in debconf für Sie nicht so wichtig, daher kann diese in diesem Fall entfernt werden.

`root@devuan:/# dpkg --purge debconf-i18n`

Da Sie nur ein gcc-base-Paket benötigen, können Sie gcc-4.8-base zugunsten von gcc-4.9-base entfernen.

`root@devuan:/# dpkg --purge gcc-4.8-base`

## APT konfigurieren, um empfohlene Pakete auszuschließen
Wie Sie vielleicht wissen, blähen empfohlene Pakete das System auf, um Features zu erhalten, die selten jemals notwendig sind. Ein weiterer Vorteil der Installation durch debootstrap ist, dass Sie sicherstellen können, dass die empfohlenen Pakete Ihr System nicht zu sehr aufblähen. Denken Sie daran, dass die meisten Browser (und wget) das Paket "ca-certificates" benötigen, um SSL-Verbindungen zu verifizieren. Daher müssen Sie dies später für alle Systeme installieren, die dies erfordern. Nicht vergessen

Verwenden Sie einen Editor, um die erforderlichen Änderungen vorzunehmen.

`root@devuan:/# sensible-editor /etc/apt/apt.conf.d/01lean`

Fügen Sie die folgenden Zeilen hinzu.

~~~
APT::Install-Recommends "0";
APT::AutoRemove::RecommendsImportant "false";
~~~

## Aktualisieren des Systems
Bevor Sie fortfahren, sollten Sie sicherstellen, dass Ihr System mit den neuesten Sicherheitspatches auf dem neuesten Stand ist.

Fügen Sie zuerst die Repositories hinzu.

`root@devuan:/# sensible-editor /etc/apt/sources.list`

Stellen Sie sicher, dass Ihre sources.list diese Zeilen enthält.

~~~
deb http://pkgmaster.devuan.org/merged jessie main
deb http://pkgmaster.devuan.org/merged jessie-updates main
deb http://pkgmaster.devuan.org/merged jessie-security main
deb http://pkgmaster.devuan.org/merged jessie-backports main
~~~

Aktualisieren Sie von den Repositorys, damit wir sie verwenden können.

`root@devuan:/# apt-get update`

Sie können jetzt ein Upgrade durchführen, um die Basispakete zu aktualisieren.

`root@devuan:/# apt-get dist-upgrade`

## Auswahl einer Debconf-Schnittstelle
Es gibt mehr als eine Schnittstelle zu debconf und Sie können zwischen ihnen auf der Grundlage Ihres Stils wählen, da noch keine installiert sind. Dies ist eine gute Möglichkeit, Ihr System anzupassen.

### Whiptail
Whiptail ist eine einfach zu bedienende Benutzeroberfläche ähnlich wie Dialog und ist die Standard-Debconf-Schnittstelle für Devuan.

### Dialog
Dialog ähnelt whiptail und ist vielen Benutzern bekannt, die zuvor textbasierte Installationen durchgeführt haben.

### Readline
Die readline-Schnittstelle ist weniger eine Schnittstelle als vielmehr eine Aufforderung zur Auswahl von Konfigurationsoptionen durch numerische Antworten. Die readline-Schnittstelle verwendet ein Perl-Modul, so dass dies eine gute Wahl ist, wenn Sie bereits andere Anwendungen für Perl haben.

### Editor
Diese Methode zum Konfigurieren von Paketen ist keine Schnittstelle, sondern Debconf öffnet einen Editor an der ersten relevanten Zeile in der Konfigurationsdatei für das installierte Paket. Dies könnte ein nützliches Werkzeug sein, um mehr über Ihr System zu erfahren oder Ihnen mehr Kontrolle zu geben, wenn es benötigt wird.

## Installieren und konfigurieren der Debconf-Schnittstelle
Eine gute Wahl für Minimalismuszwecke ist die Standard-Whiptail-Schnittstelle.

`root@devuan:/# apt-get install whiptail`

Die Readline-Schnittstelle ist eine gute Alternative für Konsolenfans.

`root@devuan:/# apt-get install libterm-readline-perl-perl`

Oder verwenden Sie die Dialogschnittstelle, wenn Sie es vor Whiptail bevorzugen.

`root@devuan:/# apt-get install dialog`

Konfigurieren Sie Debconf neu, um es wissen zu lassen, welche Schnittstelle Sie verwenden möchten.

`root@devuan:/# dpkg-reconfigure debconf`

## Hinzufügen der Netzwerkkomponenten
Installieren Sie die mindestens erforderlichen Pakete, um das Netzwerk zu erhalten.

`root@devuan:/# apt-get install netbase net-tools ifupdown`

Einige optionale Netzwerk-Tools können erwünscht sein, insbesondere ein DHCP-Client für die automatische Netzwerkkonfiguration.

`root@devuan:/# apt-get install isc-dhcp-client inetutils-ping`

## Installation des Linux-Kernels
Um von Ihrem neuen GNU/Linux-System zu booten, müssen Sie den Linux-Kernel installieren.

```root@devuan:/# apt-get install linux-image-`dpkg --print-architecture` ```

## Installation des Bootloaders
Da er eine kleinere installierte Größe und mehr Unterstützung als lilo hat, werden wir GRUB2 als Bootloader verwenden.

Wenn Sie logische Volumes verwenden, müssen Sie zuerst das lvm2-Paket installieren.

`root@devuan:/# apt-get install lvm2`

Wenn auf diesem System ein anderes Betriebssystem installiert ist und dieses gestartet werden soll, installieren Sie das os-prober-Paket.

`root@devuan:/# apt-get install os-prober`

Wir werden jetzt den Bootloader installieren.

`root@devuan:/# apt-get install grub2`

Normalerweise sollten Sie den Bootloader auf dem MBR der ersten Festplatte installieren, die meistens /dev/sda ist.

## Optionale Extras
Einige optionale Pakete für eine Basisinstallation.

`root@devuan:/# apt-get install psmisc pciutils rsyslog less`

Dies ist ein Vorschlag für Pakete, die Sie möglicherweise bei der Nachinstallation nach Bedarf benötigen.

`root@devuan:/# apt-get install man manpages lynx irssi`

Wenn Shell-Zugriff über das Netzwerk benötigt wird, vergessen Sie nicht, den Shell-Server jetzt zu installieren.

`root@devuan:/# apt-get install openssh-server openssh-client openssh-blacklist`

## Chroot-Umgebung verlassen (sauber)
Beenden Sie zuerst die Chroot-Umgebung.

`root@devuan:/# exit`

Stellen Sie sicher, dass proc, dev und sys deaktiviert sind.

`~ # umount /target/proc`

`~ # umount /target/dev`

`~ # umount /target/sys`

## Beenden der Installation
Es ist Zeit, dem Installer zu sagen, dass er die Installation jetzt beenden soll. Alle verbleibenden Konfigurationsdateien werden auf dem Zielsystem erstellt, so dass dieser Schritt nicht weggelassen werden darf.

Gehen Sie zurück zum Installationsprogramm, indem Sie Alt + F1 gleichzeitig drücken und gehen Sie zu dem Schritt, der die Installation beendet.

Sie werden aufgefordert, das Basissystem zu installieren, aber Sie sollten dies ablehnen, indem Sie die Option Zurück wählen, die diesen Schritt überspringt. Das gleiche sollte für den folgenden Bootloader-Schritt ebenso getan werden.

---

<sub>**Diese Arbeit wurde unter der Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de)] Lizenz veröffentlicht. Alle Marken sind Eigentum ihrer jeweiligen Inhaber. Diese Arbeit wird "WIE BESEHEN" zur Verfügung gestellt und kommt mit KEINERLEI Garantie.**</sub>
