# Openbox-Anleitung
Dies ist eine Einführung in openbox für diejenigen, die es noch nie zuvor benutzt haben. Es bietet ein Setup, das viele der Features von Desktop-Umgebungen bietet und dabei so minimal wie möglich ist.

## Installieren der Pakete
Wir werden mit der Installation der Mindestpakete beginnen, die für die Verwendung und Konfiguration von openbox erforderlich sind.

`root@devuan:~# apt-get install openbox obconf obmenu`

Es ist auch hilfreich, das Menüpaket zu installieren, da es Menüelemente für die Software bereitstellt, die es unterstützt.

`root@devuan:~# apt-get install menu`

Wir sollten jetzt den Compton Composite Manager installieren.

`root@devuan:~# apt-get install compton`

Das idesk-Programm kann zum Verwalten von Desktop-Symbolen und Hintergrundbildern verwendet werden.

`root@devuan:~# apt-get install idesk`

Für eine Taskleiste werden wir das schlanke tint2 verwenden.

`root@devuan:~# apt-get install tint2`

Der XFE-Dateimanager kann Laufwerke ohne ein automatisches Mounter-Backend bereitstellen, vorausgesetzt, Du hast dies in fstab eingerichtet.

`root@devuan:~# apt-get install xfe`

Standardmäßig verwendet openbox scrot, um Screenshots zu erstellen, wenn die Taste `Druck` auf der Tastatur gedrückt wird. Scrot muss nur installiert werden, damit es funktioniert.

`root@devuan:~# apt-get install scrot`

Im Abschnitt zur Konfiguration von Idesk verwenden wir den Standard-Devuan-Hintergrund, bekannt als "Purpy", der vom desktaop-base Paket bereitgestellt wird.

`root@devuan:~# apt-get install desktop-base`

## Konfiguriere Openbox
Wenn wir Openbox starten, möchten wir sicherstellen, dass unser Composite-Manager, Desktop-Manager und Taskleiste verfügbar ist. Wir müssen sicherstellen, dass das openbox-Verzeichnis in ~ / .config existiert und eine Autostart-Datei für openbox erstellen.

`user@devuan:~$ mkdir -p ~/.config/openbox`

`user@devuan:~$ editor ~/.config/openbox/autostart`

Füge in der Autostartdatei folgendes hinzu und beachte, dass jede Zeile mit `&` endet, mit Ausnahme der letzten Zeile.

~~~
compton &
idesk &
tint2
~~~

## Konfigurieren von Idesk
Um Idesk zu verwenden, müssen wir das Verzeichnis erstellen, das zum Speichern von Desktopsymbolen verwendet wird, sonst wird es nicht gestartet.

`user@devuan:~$ mkdir ~/.idesktop`

Starte Idesk, damit die Standardkonfiguration generiert wird. Du solltest den Prozess anschließend mit `ctrl + c` beenden.

`user@devuan:~$ idesk`

Ändere die Konfigurationsdatei, damit wir den Hintergrund festlegen können.

`user@devuan:~$ nano ~/.ideskrc`

Für unsere Zwecke werden wir den erwähnten Purpy Hintergrund verwenden.

~~~
Background.File: /usr/share/images/desktop-base/your-way_purpy-wide-large.png
~~~

Als nächstes werden wir ein Symbol für Xfe einrichten, das den Standard-Desktop-Link als Vorlage verwendet.

`user@devuan:~$ cp ~/.idesktop/default.lnk ~/.idesktop/xfe.lnk`

`user@devuan:~$ editor ~/.idesktop/xfe.lnk`

Ändere den neuen Desktop-Link wie folgt. Denke daran, die vertikale Position (`Y:`) des Symbols zu ändern, damit es nicht mit dem Standard überlappt.

~~~
table Icon
  Caption: Xfe
  Command: /usr/bin/xfe
  Icon: /usr/share/pixmaps/xfe.xpm
  Width: 48
  Height: 48
  X: 30
  Y: 120
end
~~~

## Verwenden des Openbox-Sitzungs-Managers
Wenn Du einen Display-Manager verwendest, kannst Du dich jetzt auf Deinem openbox-Desktop anmelden, indem Du openbox-session als Session-Manager wählst. Wenn nicht, kannst Du Deine Benutzer-xinitrc-Konfigurationsdatei verwenden und das startx-Skript aufrufen, nachdem Du dich an der Konsole angemeldet hast.

`user@devuan:~$ echo "exec openbox-session" > ~/.xinitrc`

`user@devuan:~$ startx`

Wenn Du möchtest, kannst Du den Standard-Sitzungs-Manager so ändern, dass er für alle Benutzer immer openbox-session ist.

`root@devuan:~# update-alternatives --config x-session-manager`

---

<sub>**Diese Arbeit wurde unter der Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de)] Lizenz veröffentlicht. Alle Marken sind Eigentum ihrer jeweiligen Inhaber. Diese Arbeit wird "WIE BESEHEN" zur Verfügung gestellt und kommt mit KEINERLEI Garantie.**</sub>