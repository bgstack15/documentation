# Limpando recomendados
Este documento descreve como configurar o APT para tratar recomendados como opcionais e removê-los retroativamente. Isso remove pacotes opcionais que não são desejados ou utilizados em muitos casos. Agradecimentos a [TheFlash] por apontar esse recurso do APT.

## Configurando o APT
Iremos primeiro configurar o APT para tratar pacotes recomendados como não importantes.

`root@devuan:~# editor /etc/apt/apt.conf.d/01lean`

Configure o APT de modo que pacotes recomendados sejam opcionais.

~~~
APT::Install-Recommends "0";
APT::AutoRemove::RecommendsImportant "false";
~~~

## Protegendo pacotes importantes
Embora a maioria dos pacotes recomendados não sejam importantes, existem alguns que você queira proteger contra remoção.

Para a segurança de seus navegadores e outras aplicações, devemos nos assegurar de que certificados SSL estarão sempre disponíveis ao instalar o pacote ca-certificates. Apenas pule essa etapa se souber o que está fazendo.

`root@devuan:~# apt-get install ca-certificates`

O pacote de certificados SSL será agora marcado como um pacote manualmente instalado, em vez de uma dependência ou pacote recomendado. Se a próxima etapa mostrar pacotes que você deseja manter, poderá fazer o mesmo com eles antes de confirmar qualquer remoção.

## Limpando pacotes indesejados
Agora você pode remover retroativamente todos os pacotes recomendados que se tornaram órfãos pelas mudanças de configuração.

`root@devuan:~# apt-get autoremove --purge`

Agora que removemos todos esses pacotes, não precisamos tê-los arquivados no cache.

`root@devuan:~# apt-get autoclean`

---
Tradução por Douglas H. Silva

<sub>**Esta obra está licenciada com uma Licença Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR)]. Todas as marcas são propriedades de seus respectivos donos. Este trabalho é fornecido "como é" e vem com absolutamente NENHUMA garantia.**</sub>
