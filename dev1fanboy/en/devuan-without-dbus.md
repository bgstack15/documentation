# Devuan without D-Bus
This document describes how to remove D-Bus from Devuan incorporating changing to a lightweight window manager, choosing a browser and an alternative solution to D-Bus dependent auto-mounting.

## Choosing a window manager
Unlike desktop environments most window managers do not depend on D-Bus, so you should choose to install a window manager.

* blackbox
* fluxbox
* fvwm
* fvwm-crystal
* openbox

### Installing and configuring Fluxbox
We will be using Fluxbox as it is simple and intuitive.

`root@devuan:~# apt-get install fluxbox`

Make Fluxbox the default window manager for your user when using the startx script.

`user@devuan:~@ echo "exec fluxbox" >> .xinitrc`

You can now invoke the startx script to use fluxbox.

`user@devuan:~@ startx`

A good option for a display manager is WDM.

`root@devuan:~# apt-get install wdm`

## Choosing a web browser
There are few web browsers not depending on dbus components. Some will fit your needs well and some will not.

* midori
* surf
* links2
* dillo
* lynx

One of the best known and well featured browsers not relying on dbus is midori.

`root@devuan:~# apt-get install midori`

## Removing D-Bus from Devuan
We can now remove dbus from Devuan.

`root@devuan:~# apt-get purge dbus`

We should also remove any packages orphaned by dbus removal.

`root@devuan:~# apt-get autoremove --purge`

## A simple alternative to auto-mounting
Without D-Bus you will not have auto-mounting available for most file managers, because those parts require D-Bus and simpler methods of mounting are not always implemented. We will set up mount points for ourselves, so that more sensible file managers can mount the volumes with just a few clicks.

### Manual mount points
Make a directory for the new mount point.

`root@devuan:~# mkdir /media/usb0`

Backup your fstab before proceeding.

`root@devuan:~# cp /etc/fstab /etc/fstab.backup`

Now we can edit the fstab.

`root@devuan:~# editor /etc/fstab`

We need to add a mount point for a USB drive at the end of the fstab. Be sure to set the `user` option so that non-root users can mount the drive.

~~~
/dev/sdb1       /media/usb0     auto    user,noauto     0 0
~~~

The device nodes for usb disks will vary depending on your setup. You can find out which device nodes will be used by plugging in the drive and using the `lsblk` utility.

Plug in a USB drive to test your work.

`user@devuan:~$ mount -v /media/usb0`  
`user@devuan:~$ umount -v /media/usb0`

### Choosing a file manager
For a graphical file manager that can mount and umount drives based on your fstab you can use Xfe.

`root@devuan:~# apt-get install xfe`

An interesting and minimalistic file manager is the ncurses based Midnight Commander.

`root@devuan:~# apt-get install mc`

---

<sub>**This work is released under the Creative Commons Attribution-ShareAlike 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)] license. All trademarks are the property of their respective owners. This work is provided "AS IS" and comes with absolutely NO warranty.**</sub>
