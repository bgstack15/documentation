These instructions are for migrating from the Debian Buster release only. When migrating to Beowulf the instructions are specific to the Debian release you're coming from and should be followed only as written.

# Migrate from Debian Buster to Beowulf

As of Beowulf you will lose the network-manager package when migrating. The solution here is to use the wicd network manager but if you prefer manual [network configuration](network-configuration.md) this needs to be done before continuing.

The first step is to change the sources.list to point to the Beowulf repositories.

`root@debian:~# editor /etc/apt/sources.list`

Make your sources.list look like the one provided. Comment out all other lines.

~~~
deb http://deb.devuan.org/merged beowulf main
deb http://deb.devuan.org/merged beowulf-updates main
deb http://deb.devuan.org/merged beowulf-security main
#deb http://deb.devuan.org/merged beowulf-backports main
~~~

Now you have to update the package lists from the Beowulf repositories. Recent changes in APT mean this is not allowed but we can override this behaviour.

`root@debian:~# apt-get update --allow-insecure-repositories`

The Devuan keyring should now be installed so that packages can be authenticated.

`root@debian:~# apt-get install devuan-keyring --allow-unauthenticated`

Update the package lists again so that packages are authenticated from here on in.

`root@debian:~# apt-get update`

If you want to use the wicd network manager this needs to be installed now or the upgrade will fail.

`root@debian:~# apt-get install wicd-gtk`

Upgrade your packages so that you have the latest versions. Note that this does not complete the migration.

`root@debian:~# apt-get upgrade`

Once this is done eudev needs to be installed. Note that if you're using Gnome it will be removed by this command, but can be installed again after migration.

`root@debian:~# apt-get install eudev`

The last command is known to cause package breaks but we will fix this as part of the migration process.

`root@debian:~# apt-get -f install`

A reboot is required to change sysvinit to pid1.

`root@debian:~# reboot`

Now you can perform the migration proper.

`root@debian:~# apt-get dist-upgrade`

We have migrated to Devuan so systemd related packages are not needed now.

`root@devuan:~# apt-get purge systemd libnss-systemd`

If you don't have a desktop at this point you should install one now. The default in Devuan is XFCE.

`root@devuan:~# apt-get install task-xfce-desktop`

Or you can install Gnome instead if you want to continue using it.

`root@devuan:~# apt-get install task-gnome-desktop`

You can now remove any packages orphaned by the migration process, and any unusable archives left over from your Debian install.

`root@devuan:~# apt-get autoremove --purge`  
`root@devuan:~# apt-get autoclean`

---

<sub>**This work is released under the Creative Commons Attribution-ShareAlike 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)] license. All trademarks are the property of their respective owners. This work is provided "AS IS" and comes with absolutely NO warranty.**</sub>
