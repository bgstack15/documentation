# D-Bus freie Software in Devuan
Hier wird die in Devuan enthaltene Software aufgeführt, die frei von D-Bus ist.

## Fenstermanager
Mit Vorschlägen aus der Devuan Gemeinschaft

* fluxbox
* blackbox
* openbox
* fvwm 
* fvwm-crystal
* icewm
* metacity
* evilwm
* windowmaker
* mutter
* sawfish
* icewm
* dwm
* i3
* wmaker

## Dateimanager
* xfe
* pcmanfm
* rox-filer
* mc
* gentoo
* fdclone
* s3dfm
* tkdesk
* ytree
* vifm

## Displaymanager
* nodm
* ldm
* xdm
* wdm

## Webbrowser
* chimera2
* dillo
* edbrowse
* lynx
* elinks
* links
* links2
* elinks
* w3m
* surf
* netsurf
* uzbl

## Musikspieler
* alsaplayer
* deadbeef
* cmus
* cdcd
* lxmusic
* randomplay

## Media-Player
* totem
* xine-ui
* mplayer2
* mpv
* melt

## IRC clients
* weechat
* irssi
* ekg2-ui-gtk
* scrollz
* loqui

---

<sub>**Diese Arbeit wurde unter der Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de)] Lizenz veröffentlicht. Alle Marken sind Eigentum ihrer jeweiligen Inhaber. Diese Arbeit wird "WIE BESEHEN" zur Verfügung gestellt und kommt mit KEINERLEI Garantie.**</sub>