# Migrieren zu Devuan ASCII
Dieses Dokument beschreibt, wie man von Debian Jessie oder Stretch zu Devuan ASCII migriert. Derzeit ist die Migration zu ASCII von Debian Jessie oder Stretch nicht einfach, wenn Sie GNOME oder den Netzwerk-Manager verwenden, wegen einiger gehaltener Pakete, und jede Migration ist ein wenig anders, aber dies sollte die Probleme in beiden Fällen umgehen.

## Migration vom GNOME-Desktop
Da es kein Übergangspaket gibt, müssen GNOME-Benutzer xfce4 und slim manuell installieren. Wählen Sie slim als standard Displaymanager, wenn Sie dazu aufgefordert werden.

`root@debian:~# apt-get install xfce4 slim`

Stellen Sie den Sitzungsmanager auf startxfce4 ein, damit der neue Desktop später verwendet werden kann.

`root@debian:~# update-alternatives --config x-session-manager`

## Ersetzen des Netzwerkmanagers durch Wicd
Für die Migration müssen wir wicd anstelle des Netzwerkmanagers verwenden. Aufgrund eines Kompatibilitätsproblems mit Debian **müssen** die Benutzer, die remote von Stretch migrieren, stattdessen die manuelle [Netzwerkkonfiguration](network-configuration.md) verwenden, da sonst während der Migration der Zugriff auf den Remote-Host verloren geht.

`root@debian:~# apt-get install wicd`

Stellen Sie sicher, dass der Netzwerkmanager vom Startvorgang entfernt wird.

`root@debian:~# update-rc.d -f network-manager remove`

Drahtlose Verbindungen sollten jetzt so konfiguriert werden, dass sie sich automatisch mit Wicd verbunden werden. Stoppen Sie den Netzwerkmanager, bevor Sie Ihr Netzwerk konfigurieren.

`root@debian:~# /etc/init.d/network-manager stop`

## Durchführen der Migration

Devuan verwendet standardmäßig sysvinit, also werden wir das jetzt installieren.

`root@debian:~# apt-get install sysvinit-core`

Ein Neustart ist erforderlich, um sysvinit auf pid1 umzustellen, damit wir systemd entfernen können.

`root@debian:~# reboot`

Wir können systemd jetzt ohne Beschwerden entfernen.

`root@debian:~# apt-get purge systemd`

Bearbeiten Sie die Datei sources.list, damit wir zu den Devuan-Repositories wechseln können.

`root@debian:~# editor /etc/apt/sources.list`

Fügen Sie die Devuan-Spiegel mit dem ASCII-Zweignamen hinzu. Kommentieren Sie alle anderen Zeilen aus.

~~~
deb http://deb.devuan.org/merged ascii main
deb http://deb.devuan.org/merged ascii-updates main
deb http://deb.devuan.org/merged ascii-security main
deb http://deb.devuan.org/merged ascii-backports main
~~~

Aktualisieren Sie die Paket-Indizes, damit wir den Devuan-Archivschlüsselbund installieren können.

`root@debian:~# apt-get update`

Installieren Sie den Devuan Schlüsselbund, damit Pakete ab diesem Zeitpunkt authentifiziert werden können.

`root@debian:~# apt-get install devuan-keyring --allow-unauthenticated`

Aktualisieren Sie die Paket-Indizes erneut, damit sie mit dem Schlüsselring authentifiziert werden.

`root@debian:~# apt-get update`

Schliesslich können wir nach Devuan migrieren.

`root@debian:~# apt-get dist-upgrade`

## Tätigkeiten nach der Migration
Systemd komponenten sollten jetzt aus dem System entfernt werden.

`root@devuan:~# apt-get purge systemd-shim`

Wenn Sie nicht D-Bus oder xorg verwenden, können Sie libsystemd0 möglicherweise entfernen.

`root@devuan:~# apt-get purge libsystemd0`

Löschen Sie alle verwaisten Pakete, die von Ihrer vorherigen Debian-Installation übriggeblieben sind.

`root@devuan:~# apt-get autoremove --purge`

Dies ist ein guter Zeitpunkt, um alte aus Ihrem Debian-System übrig gebliebene Pakete zu entfernen.

`root@devuan:~# apt-get autoclean`

---

<sub>**Diese Arbeit wurde unter der Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de)] Lizenz veröffentlicht. Alle Marken sind Eigentum ihrer jeweiligen Inhaber. Diese Arbeit wird "WIE BESEHEN" zur Verfügung gestellt und kommt mit KEINERLEI Garantie.**</sub>
