# Minimale Xorg-Installation
In diesem Dokument wird beschrieben, wie Du eine minimale xorg-Installation mit einigen optionalen guten Standardwerten durchführst.

## Installiere die Xorg Kern-Pakete
Melde Dich als root an einer Konsole an, indem Du das bei der Installation festgelegte Passwort verwendest.

Wir werden jetzt den minimalen Paketsatz installieren, der für xorg benötigt wird.

`root@devuan:~# apt-get install xserver-xorg-video-dummy xserver-xorg-input-void xserver-xorg-core xinit x11-xserver-utils`

Es sollte beachtet werden, dass der Treiber für den leeren Eingang sowie der Dummy-Grafiktreiber installiert sind. Diese Pakete funktionieren nicht als Treiber, erlauben aber die Installation von xorg ohne Treiber. Dies wird getan, um zu vermeiden, dass alle Treiber installiert werden, einschließlich derer, die Du nicht benötigst. Stattdessen installieren wir die einzelnen Treiber, die Du danach benötigst.

## Installieren Sie Ihren Grafiktreiber
Du solltest jetzt den Bildschirmtreiber für Deine Hardware installieren.

Anzeigetreiber für gängige Hardware sind:

* xserver-xorg-video-intel (Intel)
* xserver-xorg-video-nouveau (Nvidia)
* xserver-xorg-video-openchrome (Via)
* xserver-xorg-video-radeon (AMD)
* xserver-xorg-video-vesa (generischer Bildschirmtreiber)

Zum Beispiel solltest Du den Radeon-Treiber installieren, wenn Du einen AMD-Grafikchip hast.

`root@devuan:~# apt-get install xserver-xorg-video-radeon`

Wenn Du Dir Deines Treibers nicht sicher bist, kannst Du den vesa-Treiber für den Moment verwenden, bis Du mehr über xorg-Treiber erfährst. Er funktioniert mit allen VESA-konformen Bildschirmen und ist auch im Falle eines falsch konfigurierten Treibers nützlich.

`root@devuan:~# apt-get install xserver-xorg-video-vesa`

Für Grafikchips, die oben nicht erwähnt wurden, kannst Du im Repository nach dem richtigen Grafiktreiber für Deine Hardware suchen.

`root@devuan:~# apt-cache search xserver-xorg-video-.* | pager`

## Installiere Deine Eingabetreiber
Da es sich um eine Abhängigkeit von xserver-xorg handelt, können wir den evdev-Treiber nicht entfernen. Glücklicherweise ist evdev ein einheitlicher Treiber und für die meisten Eingabegeräte geeignet. Dies bedeutet, dass Du diesen Schritt nur befolgen musst, wenn Du spezielle Anforderungen hast.
Since it is a dependency of xserver-xorg we cannot remove the evdev driver, fortunately evdev is a unified driver and is suitable for most input devices. This means you don't need to follow this step unless you have specific requirements.

Wenn Du eine Maus und Tastatur eingerichtet hast, kannst Du diese Treiber separat installieren, wenn Du das bevorzugst.

`root@devuan:~# apt-get install xserver-xorg-input-mouse xserver-xorg-input-kbd`

Bei einem synaptics Touchpad musst Du oft den synaptics Treiber neben Deinem Tastaturtreiber installieren.

`root@devuan:~# apt-get install xserver-xorg-input-synaptics`

Wenn Du andere Eingabetreiber benötigst, kannst Du das Repository durchsuchen, um sie zu finden.

`root@devuan:~# apt-cache search xserver-xorg-input-.* | pager`

## Installieren optionaler Extras
Ich empfehle, den Grundsatz von Schriften für xorg zu installieren.

`root@devuan:~# apt-get install xfonts-100dpi xfonts-75dpi xfonts-base xfonts-scalable`

Oft möchtest Du die Unterstützung von mesa opengl mit einbeziehen, wenn Du die freien Software-Bildschirmtreiber verwendest, insbesondere wenn Du OpenGL-Spiele spielst.

`root@devuan:~# apt-get install libgl1-mesa-dri mesa-utils`

ASCII benötigt möglicherweise das Paket xserver-xorg-legacy, um Berechtigungen für den xserver zu verwalten. Beachte, dass dies ein Setuid-Wrapper ist.

`root@devuan:~# apt-get install xserver-xorg-legacy`

---

<sub>**Diese Arbeit wurde unter der Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de)] Lizenz veröffentlicht. Alle Marken sind Eigentum ihrer jeweiligen Inhaber. Diese Arbeit wird "WIE BESEHEN" zur Verfügung gestellt und kommt mit KEINERLEI Garantie.**</sub>