# Software livre do D-Bus no Devuan
Aqui estão listados programas incluídos no Devuan que são completamente independentes do D-Bus.

## Gerenciadores de Janelas
Com sugestões da comunidade do Devuan

* fluxbox
* blackbox
* openbox
* fvwm 
* fvwm-crystal
* icewm
* metacity
* evilwm
* windowmaker
* mutter
* sawfish
* icewm
* dwm
* i3
* wmaker

## Gerenciadores de Arquivos

* xfe
* pcmanfm
* rox-filer
* mc
* gentoo
* fdclone
* s3dfm
* tkdesk
* ytree
* vifm

## Gerenciadores de Exibição

* nodm
* ldm
* xdm
* wdm

## Navegadores

* chimera2
* dillo
* edbrowse
* lynx
* elinks
* links
* links2
* elinks
* w3m
* surf
* netsurf
* uzbl

## Tocadores de Música

* alsaplayer
* deadbeef
* cmus
* cdcd
* lxmusic
* randomplay

## Tocadores de Mídia

* totem
* xine-ui
* mplayer2
* mpv
* melt

## Clientes IRC

* weechat
* irssi
* ekg2-ui-gtk
* scrollz
* loqui

---
Traduzido por Douglas H. Silva

<sub>**Esta obra está licenciada com uma Licença Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR)]. Todas as marcas são propriedades de seus respectivos donos. Este trabalho é fornecido "como é" e vem com absolutamente NENHUMA garantia.**</sub>
