# Configuração de rede
Talvez você queira configurar a rede manualmente. Isso traz a vantagem de ser independente do network-manager e do dbus.

Para quem está migrando do Stretch remotamente, é necessário utilizar estas instruções ou perderá acesso ao hospedeiro.

## Conexões cabeadas
Conexões cabeadas podem ser gerenciadas pelo arquivo de configuração 'interfaces'.

`root@debian:~# editor /etc/network/interfaces`

### Configuração de rede automática
Isso irá configurar a rede automaticamente sempre que um link for detectado. Ajuste isso de acordo com o nome de sua interface, se necessário.

~~~
allow-hotplug eth0
iface eth0 inet dhcp
~~~

### Configuração estática de rede
Se você precisa de configuração estática de rede, ajuste isso para corresponder ao nome de sua interface e rede.

~~~
auto eth0
iface eth0 inet static
        address 192.168.1.2
        netmask 255.255.255.0
        gateway 192.168.1.1
~~~

## Conexões sem fio
Isso é semelhante à configuração de redes cabeadas, exceto que você terá de fornecer as credenciais da rede. Se precisar de configuração automática, poderá utilizar o gerenciador de rede wicd.

Modifique o arquivo 'interfaces' para configurar a rede sem fio.

`root@debian:~# editor /etc/network/interfaces`

Ajuste as configurações para corresponder à sua interface e configuração de rede.

~~~
allow-hotplug wlan0
iface wlan0 inet dhcp
        wpa-ssid myssid
        wpa-psk mypassphrase
~~~

---
Traduzido por Douglas H. Silva

<sub>**Esta obra está licenciada com uma Licença Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR)]. Todas as marcas são propriedades de seus respectivos donos. Este trabalho é fornecido "COMO É" e vem com absolutamente NENHUMA garantia.**</sub>
