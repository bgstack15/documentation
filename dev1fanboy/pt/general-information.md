# Informação geral
Esta página fornece informações gerais de uso sobre o Devuan. Esses são os básicos que todos devem saber ao usar Devuan, e é direcionado a quem não se sente familiarizado com o Debian. 

## Arquiteturas suportadas
* amd64
* i386
* armel
* armhf
* arm64

## Ramificações alvo
* Jessie (lançamento estável, equivalente ao Debian Jessie)
* ASCII (equivalente ao Debian Stretch)
* Ceres (equivalente ao Debian Sid)

## Diferenças ao Debian
Devuan elimina o systemd como init padrão, e não depende de systemd-shim para evitar usá-lo. A partir do lançamento ASCII estaremos usando eudev como gerenciador de dispositivos padrão em vez do udev, desde que o mesmo se tornou parte do systemd. Nós também incluímos algumas modificações únicas de nossa própria autoria, como artes e temas, software livre desenvolvido em meio a nossa comunidade e alguns pacotes interessantes para nossa base de usuários.

## Obtendo e verificando imagens de instalação
Encontre e baixe uma imagem do [files.devuan.org](https://files.devuan.org/) ou um dos espelhos. Você também deve baixar o `SHA256SUMS` e `SHA256SUMS.asc` da mesma pasta das imagens.

Primeiro verifique a integridade da imagem.

`user@GNU/Linux:~$ sha256sum --ignore-missing -c SHA256SUMS`

Agora você precisará obter as [chaves de assinatura](https://files.devuan.org/devuan-devs.gpg) dos desenvolvedores Devuan e importá-las.

`user@GNU/Linux:~$ gpg --import devuan-devs.gpg`

Agora verifique se o SHA256SUMS é o mesmo assinado pelos desenvolvedores.

`user@GNU/Linux:~$ gpg --verify SHA256SUMS.asc`

Se a assinatura retorna um bom resultado, você pode então gravar a imagem para uma mídia inicializável.

## Escrevendo uma imagem a uma mídia inicializável

Todas as imagens ISO do Devuan são híbridas e podem ser escritas a um dispositivo USB usando o dd.

`root@GNU/Linux:~# dd if=filename.iso of=/dev/sdX bs=1M && sync`

Se você não possui um dispositivo USB de sobra, você pode gravar a um CD ou DVD usando wodim.

`user@GNU/Linux:~$ wodim dev=/dev/sr0 -eject filename.iso`

### Imagens embutidas

Se você deseja escrever uma imagem embutida a um cartão SD, terá de instalar o utilitário xz.

`root@GNU/Linux:~# apt-get install xz-utils`

Descompacte a imagem.

`user@GNU/Linux:~$ xz -d <filename>.img.xz`

E então escreva-a para o cartão SD.

`root@GNU/Linux:~# dd if=filename.img of=/dev/sdX && sync`

## Alterando a ordem de inicialização da BIOS
Para que possa inicializar sua mídia de instalação talvez precise modificar a ordem de inicialização da BIOS. Não é possível cobrir essas etapas com exatidão, já que cada BIOS é diferente. Você precisa entrar em suas configurações e modificar a ordem de modo que discos USB tenham prioridade mais alta que discos rígidos. Entrar nas configurações da BIOS ou UEFI é geralmente tão simples quanto pressionar a tecla `Delete` antes da tela do POST. Alguns notebooks respondem à tecla `F2`.

## Guia de instalação
O wiki do "Friends of Devuan" disponibiliza um [guia de instalação](https://friendsofdevuan.org/doku.php/community:installing_devuan_1.0_jessie) que você pode acompanhar para instalar a partir de uma mídia inicializável. Como sempre, faça backup de seus dados antes de prosseguir!

---
Traduzido por Douglas H. Silva

<sub>**Esta obra está licenciada com uma Licença Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR)]. Todas as marcas são propriedades de seus respectivos donos. Este trabalho é fornecido "COMO É" e vem com absolutamente NENHUMA garantia.**</sub>
