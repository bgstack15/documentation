# Criptografia total de disco

Este é um guia fácil de acompanhar para configurar criptografia total de disco durante a instalação do Devuan, portanto mostraremos a forma mais rápida de se fazer isso.

Você precisará completar todas as etapas anteriores ao particionamento de discos antes de prosseguir com este guia.

## Particionando o disco

**1) Escolha utilizar todo o espaço no disco e configure LVM criptografado. Essa é a maneira mais fácil e é recomendada a iniciantes.**

![Particione com LVM e criptografia](../img/encrypt1.png)

&nbsp;

**2) Escolha o disco que será criptografado.**

![Selecione o disco a ser criptografado](../img/encrypt2.png)

&nbsp;

**3) Selecione um esquema de particionamento. Como estamos demonstrando a maneira mais fácil de conseguir criptografia total de disco, selecionaremos a opção de todos os arquivos em uma partição.**

![Selecione um esquema de particionamento](../img/partdisks2.png)

&nbsp;

**4) Você precisará escrever o esquema de partições ao disco antes que a criptografia possa ser configurada.**

![Escreva as mudanças ao disco](../img/encrypt3.png)

&nbsp;

## Criptografando o disco

**5) O instalador irá agora informar que está apagando o disco. Isso pode levar algum tempo se houver muito espaço em disco.**

![Apagando o disco com segurança](../img/encrypt4.png)

&nbsp;

**6) Agora você deve criar uma senha de criptografia. Para uma senha segura, recomendamos o mínimo de 20 caracteres, uma mistura de maiúsculas e minúsculas, pelo menos três números e no mínimo dois símbolos.**

![Defina uma senha de criptografia](../img/encrypt5.png)

&nbsp;

**7) Verifique o resumo das mudanças a serem escritas. Se estiver satisfeito, confirme-as.**

![Resumo das mudanças a serem escritas](../img/encrypt6.png)

&nbsp;

**8) Por fim, confirme as mudanças e continue com a instalação.**

![Confirme as mudanças a serem feitas](../img/encrypt7.png)

---
Traduzido por Douglas H. Silva

<sub>**Esta obra está licenciada com uma Licença Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR)]. Todas as marcas são propriedades de seus respectivos donos. Este trabalho é fornecido "COMO É" e vem com absolutamente NENHUMA garantia.**</sub>
