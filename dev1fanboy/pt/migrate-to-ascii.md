# Migrar para o Devuan ASCII
Este documento descreve como migrar para o Devuan ASCII a partir do Debian Jessie ou Stretch. Atualmente essa migração não é tão simples se você usa o GNOME ou o network-manager devido a alguns problemas com pacotes bloqueados; e cada respectiva migração é um pouco diferente, porém este documento pode ajudar a solucionar os problemas em questão.

## Migrando a partir do desktop GNOME
Por ainda não existir um pacote transicional, usuários do GNOME terão de instalar xfce4 e slim manualmente. Escolha slim como gerenciador de exibição quando questionado.

`root@debian:~# apt-get install xfce4 slim`

Defina o gerenciador de sessão para o startxfce4, assim o novo desktop poderá ser usado depois.

`root@debian:~# update-alternatives --config x-session-manager`

## Substituindo o network-manager pelo wicd

Para a migração precisamos usar o wicd em vez do network-manager. Devido a um problema de compatibilidade com o Debian, aqueles migrando do Stretch remotamente **devem** usar [configuração manual de rede](network-configuration.md), ou poderão perder acesso ao hospedeiro durante o processo de migração.

`root@debian:~# apt-get install wicd`

Remova o network-manager do processo de inicialização.

`root@debian:~# update-rc.d -f network-manager remove`

Conexões sem fio serão configuradas para conectar automaticamente com o wicd. Interrompa o network-manager antes de começar a configurar sua rede.

`root@debian:~# /etc/init.d/network-manager stop`

## Execute a migração

Devuan utiliza o sysvinit por padrão, então instalaremos ele agora.

`root@debian:~# apt-get install sysvinit-core`

A reboot is required to switch sysvinit to pid1 so we can remove systemd.
Uma reinicialização é necessária para definir o sysvinit como pid1, substituindo o systemd.

`root@debian:~# reboot`

Agora podemos remover o systemd sem problemas.

`root@debian:~# apt-get purge systemd`

Modifique o arquivo sources.list para que possamos apontá-lo para os repositórios Devuan.

`root@debian:~# editor /etc/apt/sources.list`

Adicione os espelhos do Devuan apontando o ASCII como ramificação. Desabilite quaisquer outras linhas.

~~~
deb http://deb.devuan.org/merged ascii main
deb http://deb.devuan.org/merged ascii-updates main
deb http://deb.devuan.org/merged ascii-security main
deb http://deb.devuan.org/merged ascii-backports main
~~~

Atualize o índice de pacotes para que possamos instalar o chaveiro do Devuan.

`root@debian:~# apt-get update`

Agora instale o chaveiro do Devuan passando um parâmetro que ignora a autenticação apenas durante essa execução.

`root@debian:~# apt-get install devuan-keyring --allow-unauthenticated`

Update the package indexes again so that they are authenticated with the keyring.
Atualize o índice de pacotes novamente para que os pacotes sejam autenticados com o chaveiro a partir de agora.

`root@debian:~# apt-get update`

Finalmente podemos migrar para o Devuan.

`root@debian:~# apt-get dist-upgrade`

## Tarefas pós migração
Componentes systemd já podem ser removidos.

`root@devuan:~# apt-get purge systemd-shim`

Se você não estiver usando D-Bus ou xorg, poderá remover libsystemd0.

`root@devuan:~# apt-get purge libsystemd0`

Elimine quaisquer pacotes órfãos deixados da instalação do Debian.

`root@devuan:~# apt-get autoremove --purge`

Essa é uma boa hora de eliminar o antigo arquivo de pacotes deixados pelo APT da sua instalação anterior.

`root@devuan:~# apt-get autoclean`

---
Traduzido por Douglas H. Silva

<sub>**Esta obra está licenciada com uma Licença Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR)]. Todas as marcas são propriedades de seus respectivos donos. Este trabalho é fornecido "COMO É" e vem com absolutamente NENHUMA garantia.**</sub>
