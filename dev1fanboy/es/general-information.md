# Información general
Esta página provee información general acerca del uso de Devuan. Esta es la información básica que todo el que
desee utilizar Devuan debe conocer, y está orientada para cualquiera que no esté familiarizado con Debian.

## Arquitecturas soportadas
* amd64
* i386
* armel
* armhf
* arm64

## Versiones disponibles
* ASCII (equivalente a Debian Stretch)
* Beowulf (versión estable, equivalente a Debian Buster)
* Ceres (equivalente a Debian Sid)

## Diferencias con Debian
Devuan elimina a systemd del proceso de inicio (*init*) por defecto, evitando el uso de systemd-shim para tal fin y 
usando elogind para proporcionar algunas funciones que de otra manera necesitan systemd.
A partir de ASCII se utiliza eudev como gestor de dispositivos por defecto en lugar de udev, ya que ahora es parte de
los fuentes de systemd. También incluye una serie de cambios propios tales como el arte y los temas de escritorio,
software libre desarrollado por la comunidad Devuan y algunos paquetes adecuados para la base de usuarios de Devuan.

## Obtener y descargar las imágenes de instalación
Buscar y descargar las imágenes desde [files.devuan.org](https://files.devuan.org/) o cualquiera de los *mirrors*.
Además se deben descargar los archivos `SHA256SUMS` y `SHA256SUMS.asc` desde el mismo directorio de las imágenes.

Primero verificar la integridad de las imágenes de Devuan.

`user@GNU/Linux:~$ sha256sum --ignore-missing -c SHA256SUMS`

Luego se necesita obtener e importar las [claves de cifrado](https://files.devuan.org/devuan-devs.gpg) de los
desarrolladores de Devuan.

`user@GNU/Linux:~$ gpg --import devuan-devs.gpg`

Verificar que el archivo SHA256SUMS sea firmado por los desarrolladores de Devuan.

`user@GNU/Linux:~$ gpg --verify SHA256SUMS.asc`

Si la firma es auténtica es posible escribir la imagen en un medio arrancable.

## Escribir la imagen en un medio arrancable

Todas las imágenes de Devuan son híbridas y pueden ser grabadas a un medio USB utilizando dd.

`root@GNU/Linux:~# dd if=filename.iso of=/dev/sdX bs=1M && sync`

También se pueden grabar en un CD o DVD utilizando wodim.

`user@GNU/Linux:~$ wodim dev=/dev/sr0 -eject filename.iso`

### Imágenes embebidas

Si se desea grabar una imagen embebida en una tarjeta SD, se necesita instalar el utilitario xz.

`root@GNU/Linux:~# apt-get install xz-utils`

Descomprimir la imagen.

`user@GNU/Linux:~$ xz -d <filename>.img.xz`

Luego escribirla en la tarjeta SD.

`root@GNU/Linux:~# dd if=filename.img of=/dev/sdX && sync`

Si tu tarjeta es sunxi entonces también escribir la [imagen u-boot](https://files.devuan.org/devuan_beowulf/embedded/u-boot/) a la tarjeta SD.

`root@GNU/Linux:~# dd if=<imagename>.bin of=/dev/sdX bs=1024 seek=8 && sync`

## Cambiar el orden de inicio en el BIOS

A fin de iniciar desde un medio de instalación tal vez sea necesario alterar el orden de *boot* a nivel BIOS. No es posible
cubrir exactamente los pasos necesarios para hacer esto ya que cada BIOS es diferente. Se debe ingresar a la interfaz
del BIOS o UEFI y cambiar el orden de inicio para que los dispositivos CD/DVD-ROM y USB queden antes que los discos
rígidos. Ingresar a la configuración del BIOS es tan simple como presionar la tecla `Supr` o `Del` (`Delete`) antes del
POST.

## Guía de instalación

Si estás interesado en instalar Devuan y has grabado tu imagen a un CD/DVD, es posible seguir la [guía de instalación de Devuan](devuan-install.md#instalar-devuan) para tener tu sistema listo y corriendo Devuan.

---

<sub>Traducido por Emiliano Marini.</sub>

<sub>**Este trabajo es liberado bajo la licencia Atribución-CompartirIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.es)]. Todas las marcas registradas son propiedad de sus respectivos dueños. Este trabajo se provee "COMO TAL" y NO posee garantía en absoluto.**</sub>

