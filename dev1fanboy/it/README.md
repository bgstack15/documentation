# Installazione, aggiornamento e minimalismo di Devuan GNU/Linux
Questa documentazione contiene informazioni sull'installazione e l'aggiornamento di Devuan, le procedure di migrazione verso Devuan partendo da altri sistemi e consigli su come ottenere un sistema minimale. La wiki è disponibile anche in altre lingue.

## Tutti i rilasci
[Informazioni generali](general-information.md)  
[Installa Devuan](devuan-install.md)  
[Crittaggio totale del disco](full-disk-encryption.md)  
[Configurazione della rete](network-configuration.md)  
[Devuan senza D-Bus](devuan-without-dbus.md)  
[Software libero da D-Bus](dbus-free-software.md)  
[Installazione minimale di xorg](minimal-xorg-install.md)  
[Installazione minimale di xfce](minimal-xfce-install.md)  

## ASCII
[Aggiornare ad ASCII](upgrade-to-ascii.md)  
[Migrare verso ASCII](migrate-to-ascii.md)

## Beowulf
[Aggiornare a Beowulf](upgrade-to-beowulf.md)  
[Migrare da Debian Buster](buster-to-beowulf.md)  
[Migrare da Debian Stretch](stretch-to-beowulf.md)  
[Migrare da Debian Jessie](jessie-to-beowulf.md)

## Traduzioni

Sono disponibili queste traduzioni grazie al team wiki.

[Read in English](../en) **|** [Auf Deutsch lesen](../de) **|** [Διαβάστε στα Ελληνικά](../el) **|** [Leer en Español](../es) **|** [Leia em Português](../pt)

---

<sub>Traduzione italiana di antoniotrkdz<sub>

<sub>**Questa guida è rilasciata in base alla licenza Creative Commons Attribuzione - Condividi allo stesso modo 4.0 Internazionale [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.it)]. Tutti i marchi sono proprietà dei rispettivi depositari. Le informazioni in questa guida vengono fornite "COME SONO" e non sono coperte da alcuna garanzia.**</sub>
