# Guida all'installazione di Devuan

Questa guida descrive in modo semplice come installare Devuan su hardware supportato partendo dai file immagine CD/DVD disponibili. Ovviamente è caldamente consigliato fare un backup dei dati prima di iniziare.

## Contenuti
[Prerequisiti](#prerequisiti)  
[Architetture Supportate](#architetture-supportate)  
[File immagine](#file-immagine)  
[Installazione di Devuan](#installazione-di-devuan)

## Prerequisiti

Si dovrebbe almeno sapere come riversare un'immagine ISO su CD/DVD o su dispositivi di archiviazione USB e come avviarci il computer. Verrà qui descritta la procedura per coloro che già usano GNU/Linux o simili.

## Architetture supportate

* amd64
* i386

## File Immagine

I file immagine per l'installazione possono essere ottenuti:

* Direttamente dall'[archivio dei rilasci Devuan](https://files.devuan.org)
* Dai mirror elencati in [devuan.org](https://www.devuan.org) più vicini.
* Via [torrent](https://files.devuan.org/devuan_beowulf.torrent) per le versioni stabili

Se si dispone di accesso alla rete si suggerisce l'installazione usando l'immagine DVD singola, altrimenti usate il set completo.

Si prega di usare i mirror o i torrent se possibile.

Coloro che non usano la linea di comando possono andare direttamente all'[installazione](#installazione-di-devuan).

### Verifica dell'integrità dei file immagine

Prima di scrivere un file immagine su dispositivo rimovibile, è meglio verificarne l'integrità per essere sicuri che non sia corrotta. Questo passaggio evita l'insorgenza di potenziali problemi nelle successive fasi dell'installazione.

Scaricate `SHA256SUMS` dall'[archivio dei rilasci Devuan](https://files.devuan.org) e verificate l'integrità del file immagine.

`user@hostname:~$ sha256sum --ignore-missing -c SHA256SUMS`

### Verifica dei file immagine

I file immagine distribuiti da Devuan sono firmati digitalmente in modo da garantirne la provenienza. La verifica garantisce che non siano stati alterati prima di riceverli.

Ottenete le [chiavi](https://files.devuan.org/devuan-devs.gpg) degli sviluppatori Devuan e aggiungetele al vostro "portachiavi".

`user@hostname:~$ gpg --import devuan-devs.gpg`

Usate `SHA256SUMS.asc` dall'archivio per verificare i file immagine.

`user@hostname:~$ gpg --verify SHA256SUMS.asc`

Un risultato positivo indica che tutto è in ordine.

### Scrittura del file immagine su CD/DVD o supporto USB

I file immagine possono essere scritti su CD o DVD usando wodim.

`user@hostname:~$ wodim dev=/dev/sr0 -eject filename.iso`

Tutti i file immagine Devuan sono ISO ibride e possono essere scritte su supporto USB usando dd.

`root@hostname:~# dd if=filename.iso of=/dev/sdX bs=1M && sync`

## Installazione di Devuan

Per l'installazione utilizzeremo l'installatore testuale, che accetta solo input da tastiera. Sebbene questo possa spaventare i novizi, non c'è nulla da temere perchè la procedura è guidata dall'inizio alla fine (Le immagini sono relative alla procedura d'installazione in inglese).

&nbsp;

**1) Avviate dal CD/DVD o dal supporto USB, e scegliete l'opzione `Install`.**

![Primo avvio installazione di Devuan](../img/01start.png)

&nbsp;

**2) I passaggi seguenti impostano il linguaggio, il paese e il layout della tastiera.**

![Layout della tastiera](../img/02keyboard.png)

&nbsp;

**3) L'installatore configurerà automaticamente la connessione, gli utenti di connessioni senza fili  
dovranno indicare il SSID e fornire la password di accesso.**

**In seguito verrà richiesto di impostare un hostname per il sistema. Siate creativi ma ricordatevi  
di non usare spazi o caratteri speciali.**

![Nome dell'host](../img/03hostname.png)

&nbsp;

**4) Verrà chiesto di impostare anche un nome di dominio. Se non ne avete bisogno o non ne siete  
a conoscenza potete lasciare il campo vuoto.**

![Nome di Dominio](../img/04domain.png)

&nbsp;

**5) Si raccomanda di usare una password di root per Devuan. È buona norma impostarne  
una difficile da indovinare. È richiesta la conferma in modo da garantirne la correttezza.**

![Password di Root](../img/05rootpw.png)

&nbsp;

**6) Verrà richiesto di configurare un account utente. A meno che non ci sia una diversa necessità  
lasciate vuoto il campo nome completo e proseguite con l'immissione del nome utente.**

![Nome utente](../img/06username.png)

&nbsp;

**7) Come per root è richiesta l'immissione della password e la sua conferma.**

![Password utente](../img/07userpw.png)

&nbsp;

**8) L'installatore imposterà l'orologio per mezzo di [NTP](https://en.wikipedia.org/wiki/Network_Time_Protocol). immettete le informazioni relative al fuso  
orario e continuate.**

![Configurazione dell'orologio](../img/08ntp.png)

&nbsp;

**9) Prima di poter installare Devuan si deve partizionare il disco. Se possibile è raccomandato  
l'uso dello spazio contiguo maggiore. Le partizioni già esistenti rimarranno inalterate.  
Altrimenti se non ci sono dati da salvare si può scegliere di usare l'intero disco.**

**Se fosse necessario il crittaggio dell'intero disco si prega di seguire [questa guida](../full-disk-encryption.md) prima di continuare.**

![Usa il maggior spazio contiguo](../img/09part1.png)

&nbsp;

**10) Installare tutto in una singola partizione è la scelta migliore per i novizi.  
Le descrizione della procedura di partizionamento manuale esula dall'oggetto di questa guida.**

![Tutto in un'unica partizione](../img/10part2.png)

&nbsp;

**11) È il momento di scrivere le partizioni su disco e di formattarle con i file system. Se siete soddisfatti  
delle modifiche scegliete di scriverle su disco. Sarà richiesta conferma prima di continuare.**

![Scrivi le modifiche su disco](../img/11part3.png)

&nbsp;

**12) Viene installato Il sistema base. il tempo richesto varia a seconda delle specifiche hardware.**

![Attendere la fine dell'installazione del sistema](../img/12install.png)

&nbsp;

**13) In caso siate privi di connessione Internet o non vogliate usare un mirror, si raccomanda di  
inserire ulteriori CD così da avere più pacchetti a disposizione per l'installazione.**

![inserisci un altro CD](../img/13scancd.png)

&nbsp;

**14) Se state installando da un'immagine CD/DVD completa, l'installatore chiederà se si desidera usare  
un mirror di rete. Scelta raccomandata se si dispone di accesso alla rete in quanto si otterranno le  
versioni più aggiornate dei pacchetti. Per un'installazione offline continuate senza un mirror.**

![Usa un mirror](../img/14mirror1.png)

&nbsp;

**15) Uno qualunque tra i mirror proposti andrà bene. Qualora ci fosse un mirror più vicino vi si verrà  
rediretti in fase di installazione dei pacchetti.**

![Usa un mirror](../img/15mirror2.png)

&nbsp;

**16) Devuan può usare popcon per reperire informazioni sui pacchetti più usati. Questa è una scelta  
a discrezione dell'utente e reperisce solo dati relativi a quali pacchetti verranno installati.**

![PopCon](../img/16popcon.png)

&nbsp;

**17) La preselezione proposta permette di ottenere un'ambiente desktop XFCE funzionante.  
Notate bene che non è necessario selezionare XFCE4 in quanto è il default, ma potreste voler optare  
per un altro desktop. Altri valori possono essere inclusi od esclusi dalla selezione in base alle vostre  
esigenze.**

![Selezione del software](../img/17tasksel.png)

&nbsp;

**18) L'installatore farà il suo lavoro per i pacchetti selezionati. Quest'operazione richiederà  
un po' di tempo.**

![Installazione](../img/18retrieve.png)

&nbsp;

**19) Viene richiesto di scegliere un sistema di init. In Devuan il default è sysvinit.**

![Scegli un sistema init](../img/19init.png)

&nbsp;

**20) Alla fine dell'installazione dei pacchetti, verrà installato il bootloader GRUB. Questo  
permetterà al computer di avviare il sistema operativo dopo l'installazione. Se vi venisse  
proposto di installarlo nell'area MBR allora scegliete quest'opzione.**

**Alcune configurazioni non richiederanno ulteriori impostazioni e la procedura terminerà.**

![Installazione del bootloader grub](../img/20grub1.png)

&nbsp;

**21) È importante scegliere la corretta collocazione del bootloader. Non dovrebbe essere installato  
su di una partizione, ma nell'area MBR del disco rigido.**

**In questo caso `/dev/sda` è l'unico disco rigido ed ove avverrà l'installazione.**

![Scelta della collocazione del bootloader](../img/21grub2.png)

&nbsp;

**22) La procedura è terminata, rimuovete il supporto d'installazione per riavviare ed iniziare ad  
usare il sistema Devuan appena installato.**

![Fine dell'installazione](../img/22finish.png)


---

<sub>Traduzione di antoniotrkdz<sub>

<sub>**Questa guida è rilasciata in base alla licenza Creative Commons Attribuzione - Condividi allo stesso modo 4.0 Internazionale [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.it)]. Tutti i marchi sono proprietà dei rispettivi depositari. Le informazioni in questa guida vengono fornite "COME SONO" e non sono coperte da alcuna garanzia.**</sub>
