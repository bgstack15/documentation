Questa guida descrive come aggiornare un'installazione Devuan pre-esistente a Devuan Beowulf. Non dovrebbe essere usata per le migrazioni da Debian.

# Aggiornare a Beowulf

Prima di tutto bisogna modificare il file sources.list di modo che punti ai repository Beowulf.

`root@devuan:~# editor /etc/apt/sources.list`

Modificate sources.list come descritto qui di seguito. Commentate tutto il resto.

~~~
deb http://deb.devuan.org/merged beowulf main
deb http://deb.devuan.org/merged beowulf-updates main
deb http://deb.devuan.org/merged beowulf-security main
#deb http://deb.devuan.org/merged beowulf-backports main
~~~

Aggiornate la lista dei pacchetti dai repository Beowulf.

`root@devuan:~# apt-get update`

Gli utenti di Devuan Jessie dovrebbero installare la versione più recente del "portachiavi" dei repository Devuan e aggiornare nuovamente la lista dei pacchetti in modo da autenticarli.

`root@devuan:~# apt-get install devuan-keyring`  
`root@devuan:~# apt-get update`

Se lo xscreensaver fosse attivo va "killato" in quanto deve essere inattivo per poter essere aggiornato.

`root@devuan:~# killall xscreensaver`

Procedete ora all'aggiornamento.

`root@devuan:~# apt-get dist-upgrade`

In caso di problemi con qualunque pacchetto dovrete risolvere il problema relativo e ripartire con l'aggiornamento.

`root@devuan:~# apt-get -f install`  
`root@devuan:~# apt-get dist-upgrade`

Gli utenti che provengono da ASCII e usano upower dovranno regredire i loro pacchetti upower al fine di evitare problemi come il [bug #394](https://bugs.devuan.org/cgi/bugreport.cgi?bug=394).

`root@devuan:~# apt-get install --allow-downgrades upower/beowulf gir1.2-upowerglib/beowulf`

Potreste voler rimuovere i pacchetti resi orfani dalla procedura e i pacchetti obsoleti.

`root@devuan:~# apt-get autoremove --purge`  
`root@devuan:~# apt-get autoclean`

---

<sub>Traduzione di antoniotrkdz<sub>

<sub>**Questa guida è rilasciata in base alla licenza Creative Commons Attribuzione - Condividi allo stesso modo 4.0 Internazionale [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.it)]. Tutti i marchi sono proprietà dei rispettivi depositari. Le informazioni in questa guida vengono fornite "COME SONO" e non sono coperte da alcuna garanzia.**</sub>
